#!/bin/bash
#----------------
#lines.sh
#(c) H.Buchmann FHNW 2008
#output
# nol file eol
# nol: number of lines
# eol: endofline
#----------------
dir=${1}
if [ -z ${dir} ] 
   then dir="."
fi
find ${dir} -type f -exec wc -l {} \;

