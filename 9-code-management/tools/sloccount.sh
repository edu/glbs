#!/bin/bash
#---------------------
#sloccount.sh
#(c) H.Buchmann FHNW 2017
# we are in 9-code-management
#---------------------
LINUX_HOME=~/resources/beaglebone/linux/
#generate first time from source
#sloccount  --datadir sloc -- ${LINUX_HOME}
#already done

sloccount  --details --cached --datadir sloc -- ${LINUX_HOME}
#sloccount  --cached --datadir sloc -- ${LINUX_HOME}
