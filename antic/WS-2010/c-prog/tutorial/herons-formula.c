/*
---------------------------
herons-formula
(c) H.Buchmann FHNW 2006 
$Id$
---------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static void usage()
{
 printf("usage: herons-formula sideA sideB sideC\n"
        "   sideA|B|C floating numbers\n"
       );
 exit(1); /* with error code */ 
}

/* global variables */

double sideA=0;
double sideB=0;
double sideC=0;
double s=0;

double readSide(char* name,char* arg)
{
 char* endP;
 double s=strtod(arg,&endP);
 if (arg==endP) 
    {
     printf("side%s not floating number (%s)\n",name,arg);
     usage();
    }
 if (s<0) 
    {
     printf("side%s is negative\n",name);
     usage();
    }   
 return s;   
}

double angle(double s0,double s1)
{
 /* your work */
 return 0;
}


/*
 args[0] name of program
 args[1] sideA floating number
 args[2] sideB floating number
 args[3] sideC floating number
*/

int main(unsigned argc,char** args)
{
 if (argc!=4) usage();
 sideA=readSide("A",args[1]);
 sideB=readSide("B",args[2]);
 sideC=readSide("C",args[3]);
 s=0.5*(sideA+sideB+sideC);
 double alfa =angle(sideB,sideC);
 double beta =angle(sideC,sideA);
 double gamma=angle(sideA,sideB);
 printf("alfa=%f beta=%f gamma=%f\n",alfa,beta,gamma);
 return 0;  
}
