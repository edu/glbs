/*
---------------------------
ascii-table
(c) H.Buchmann FHNW 2006
$Id$
---------------------------
*/
#include <stdio.h>
#define COL 8
int main(unsigned argc,char** args)
{                      /* 0 1 2 3 4 5 6 7 8 a b c d e f */
 printf("\\begin{tabular}{l|");
 for(unsigned i=0;i<COL;++i)printf("|l");
 printf("}\n");
 unsigned ch=0; 
 unsigned col=0;
 printf("&");
 for(unsigned i=0;i<COL;++i) 
 {
  if (i>0) printf("&");
  printf("%02x",i);
 }
 printf("\\\\\n"
        "\\hline\\hline\n");

 while(ch<128)
 {
  if (col==0) printf("%02x",ch);
  printf("&\\symbol{%d}",ch);  
//   if (ch>=' ') 
//      else      printf("&.");
  ++col;   
 if (col==COL)
     {
      printf("\\\\\n"
             "\\hline\n"
            );
      col=0;
     }
   ++ch;   
 }
 printf("\\end{tabular}\n");
}
