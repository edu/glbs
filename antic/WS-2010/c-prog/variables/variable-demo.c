/*---------------------
variable-demo
(c) H.Buchmann FHNW 2006
$Id$
-----------------------*/
#include <stdio.h>

int main(unsigned argc,char** args)
{
 unsigned val=8; /* definition and initialization */
 /* |     |   |-- initial value
    |     |------ name  
    |------------ how to interpret the bits
 */
 printf("           val= %u\n"
        "address of val= %p\n"
	"   as unsigned= %u\n"
	"        as hex= %x\n",val,&val,&val,&val); 
	                         /*|------------ address of */   

 unsigned* valP=&val;  /* definition initialized */
 *valP=5;
 printf("           valP= %p\n"
        "address of valP= %p\n"
	"          *valP= %u\n"
	"            val= %u\n",
	valP,&valP,*valP,val);
	         /*|--------------------- dereferenced */
 return 0;
}

