/*--------------------------
array-pointer
(c) H.Buchmann FHNW 2006
$Id$
----------------------------*/
#include <stdio.h>

int main(unsigned argc,char** args)
{
 unsigned arr[5]; /* defined but not initialized */
 printf("address of arr= %p\n"
        "  value of arr= %p\n",
        &arr,arr);
	
 unsigned* valP=arr;
 /* and not 
  unsigned* valP=&arr;
 */
 
 /* array as pointer */
 unsigned val=100;
 for(unsigned i=0;i<5;++i)
 {
  *(arr+i)=val;
  --val;
 }
 
 /* pointer as array */
 for(unsigned i=0;i<5;++i)
 {
  printf("%5u %u\n",i,valP[i]);
 }
 return 0;
}
