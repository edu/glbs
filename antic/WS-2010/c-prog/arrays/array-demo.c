/*--------------------------
array-demo
(c) H.Buchmann FHNW 2006
$Id$
---------------------------*/
#include <stdio.h>

int main(unsigned argc,char** args)
{
 unsigned arr[]={500,400,300,200,100};
           /*|---------------------------- size optional */
 
/* read access */
 for(unsigned i=0;i<5;++i)
 {
  printf("%u\n",arr[i]);
 }
/* write to array*/
 unsigned v=5;
 for(unsigned i=0;i<5;i++)
 {
  arr[i]=v;
  v--;
 }
/* show again */
 for(unsigned i=0;i<5;++i)
 {
  printf("%u\n",arr[i]);
 }
 
 return 0;
}
