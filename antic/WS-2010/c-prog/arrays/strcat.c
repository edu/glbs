/*--------------------------------
  strcat
  semantics see manual page of strcpy
  (c) H.Buchmann FHNW 2006
  $Id$
----------------------------------*/
#include <stdio.h>

/*assuming there is enough space in dst */
/*assuming no intersection dst+strlen(dst) 
                           src+strlen(src) */
/* TODO
 handle overlapping see strcpy
*/
void strcat(char* dst,char* src)
{
 /* go to the end of dst strlen*/
 unsigned i=0; /* index in dst */
 while(dst[i]) ++i;
 /* dst[i] terminating zero
 /* copy src */
 unsigned j=0; /* index in src */ 
 while(1)
 {
  char ch=src[j++]; 
  dst[i++]=ch;
  if (ch=='\0') break;
 }
}

void strncat(char* dst,char* src,unsigned cap)
                                     /* cap capacity of dst */
{
  /* go to the end of dst strlen*/
 unsigned i=0; /* index in dst */
 while(dst[i]) ++i;
 unsigned j=0;
 while(1)
 {
  if (i==cap) break;         /* dst too small */
  char ch=src[j++];
  dst[i++]=ch;
  if (ch=='\0') break;
 }
}
/* TODO 
 change semantic of cap:
  cap: the maximal number of chars to be copied from src
*/

#define SIZE 80
int main(unsigned argc,char** args)
{
 char src[]  ="1234567"; /* the source */
 char dst[SIZE];           /* the destination not initialized */
 dst[0]='\0';            /* empty string for good start*/
 strncat(dst,src,SIZE);
 printf("src= %s\n"
        "dst= %s\n",src,dst);
 strncat(dst,"abcde",SIZE);
 printf("src= %s\n",dst); 	
 return 0;
}
