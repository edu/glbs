#ifndef THREAD_H
#define THREAD_H
//------------------------
//thread for c++ see java threads
//(c) H.Buchmann FHNW 2006
//$Id$
//------------------------
#include <pthread.h>


//------------------------------- Mutex
class Mutex
{
 private:
  pthread_mutex_t mutex;
 public:
           Mutex();
  virtual ~Mutex();
  void lock();
  void unlock();
};

//------------------------------- Runnable
class Runnable
{
 public:
  virtual ~Runnable();
  virtual void run()=0;
};

//------------------------------- Thread
class Thread
{
 private:
  ::pthread_t th;
  Runnable* run;

  Thread(const Thread&); //no copy 
  static void* start(void* thread);
  
 public:
           Thread(Runnable*);
  virtual ~Thread();
  void start();
};

#endif
