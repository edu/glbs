unsigned val=0;  //accessible by everybody

void* run(void* p)
{
 unsigned id= *(unsigned*)p;
 printf("init %d\n",id);
 while(1)
 {
  unsigned l=val; //copy 
  l++;
  if (l!=val+1) printf("-- %d\n",id);
  val=l;
 }
 return 0; //never called
}
