void f2(...)
{
 
}

void f1(...)
{
 /* do something */
 f2(..);
}

int main(unsigned argc,char** args)
{                /* start */
 f1(...);
 f2(...);
 return 0;       /* end */
}
