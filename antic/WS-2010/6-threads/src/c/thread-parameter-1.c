/*--------------
 thread-parameter-1
 (c) H.Buchmann FHNW 2007
 $Id$
----------------*/
#include <stdio.h>
#include <pthread.h>

unsigned id=0; /* global access */
pthread_mutex_t initM; /* defined but not initialized */

void* runIt(void* p)  
         /* p points to an unsigned */
{
 unsigned localId= *(unsigned*)p; /* copy into localId*/
 pthread_mutex_unlock(&initM);
 printf("localId=%u\n",localId);
 while(1)
 {
 }
}


int main(unsigned argc,char** args)
{
 pthread_mutex_init(&initM,0);
#define SIZE 20 
 
 pthread_t th[SIZE];
 for(unsigned i=0;i<SIZE;++i)
 {
  pthread_mutex_lock(&initM);
  id++;		
  pthread_create(
      &th[i],         /* the handle */
      0,            /* default attributes */
      runIt,
      &id           /* pointer to unsigned id */
        	);
 }
 getc(stdin);
 return 0;
}
 
