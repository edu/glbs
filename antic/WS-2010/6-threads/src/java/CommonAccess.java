//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2008
//$Id$
//----------------
class Pool
{
 public int v=0;
}

class MyRun implements Runnable 
{
 private Pool   pool;  //reference to Pool 
 private Thread th=new Thread(this);
 private int    id;
 private int    cnt0,cnt1;
 MyRun(Pool pool,int id,int cnt0,int cnt1)
 {
  this.pool=pool;
  this.id=id;
  this.cnt0=cnt0;
  this.cnt1=cnt1;
  th.start();     //of thread
 }
 
 void wait(int cnt)
 {
  while(cnt>0) --cnt;
 }
 
 public void run()
 {
 
  while(true)
  {
   int cnt0=this.cnt0;while(cnt0>0)--cnt0;
//   wait(cnt0);
   int l=pool.v; //local copy
   ++l;
//   wait(cnt1);
   int cnt1=this.cnt1;while(cnt1>0)--cnt1;
   if (l!=(1+pool.v)) System.err.println("-o-"+id);
   pool.v=l;
  }
 }
}

class CommonAccess
{
 static final int LONG =(1<<14);
 static final int SHORT=1;

 public static void main(String args[]) throws Exception
 {
  
  Pool pool =new Pool();    //one instance
                            //cnt0 cnt1
  MyRun run0=new MyRun(pool,0,LONG,SHORT);
  MyRun run1=new MyRun(pool,1,SHORT,SHORT); 
 }
}
