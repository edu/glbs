#------------------
#producer.sh
#(c) H.Buchmann FHNW 2009
#$Id$
#------------------
if [ -z ${1} ]
  then echo "usage ${0} fifo"
       exit 1
fi
COUNT=0
while [ true ]
do
 echo "produce ${COUNT}" > ${1} #redirection stdout -> ${1}
 
 COUNT=$((COUNT+1))
done
 
