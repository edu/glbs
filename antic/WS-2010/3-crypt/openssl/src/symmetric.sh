#-------------------
#symmetric.sh
#(c) H.Buchmann FHNW 2010
#$Id$
#-------------------
CIPHER=bf
K=01234567
IV=0123

case ${1} in
 enc*)
  openssl enc -${CIPHER} -K ${K} -iv ${IV} -e
 ;;
 dec*)
  openssl enc -${CIPHER} -K ${K} -iv ${IV} -d
 ;;
 *)
  echo "usage ${0} enc|dec"
  echo " src: stdin"
  echo " dst: stdout"
 ;;
  
esac
