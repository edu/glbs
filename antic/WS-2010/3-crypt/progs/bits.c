/*-----------------------
  bits
  bits -> char
  char -> bits
  (c) H.Buchmann FHNW 2008
  $Id$
-------------------------*/
#include "bits.h"
#include <stdio.h>

static const char Bits[]=
{
 1<<0x7,1<<0x6,1<<0x5,1<<0x4,1<<0x3,1<<0x2,1<<0x1,1<<0x0
};

void block2char(char* blk,         /* space for 64 chars */
                char* data         /* space for  8 chars */
		)
{
 unsigned i=0;                           /* index in blk */
 for(unsigned k=0;k<8;++k)            /* k index in data */
 {
  char d=0;                             /* the k-th byte */
  for(unsigned l=0;l<8;++l)           /* bits in data[k] */
  {
   
   if (blk[i]) d=d|Bits[l]; 
   i++;                             /* next bit from blk */
  }
  data[k]=d;
 }
}

void char2block(char* data,         /* space for 8 chars */
                char* blk          /* space for 64 chars */
	       )
{
 unsigned i=0;                           /* index in blk */ 
 for(unsigned k=0;k<8;++k)            /* k index in data */
 {
  char d=data[k];                       /* the k-th byte */
  for(unsigned l=0;l<8;++l)           /* bits in data[k] */
  {
   blk[i]=(d&Bits[l])?1:0;
   i++;                               /* next bit to blk */
  }
 }
}

