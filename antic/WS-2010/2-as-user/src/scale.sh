#----------------
#scale.sh
#(c) H.Buchmann FHNW 2009
#$Id$
#-----------------
SAMPLING=44100
SCALE="466.16/1  493.88/2  523.25/0.5"
DUR=0.2
for f in ${SCALE} 
do
 ./sin $SAMPLING $(dirname $f) $(basename $f)| \
   play -r$SAMPLING -s -4 -traw -
done
