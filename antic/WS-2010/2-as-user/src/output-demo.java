// -----------------------
// output-demo
// (c) H.Buchmann FHNW 2009
// $Id$
// -------------------------
class OutputDemo
{
 public static void main(String args[])
 {
  System.out.println("Hello on STDOUT\n");
  System.err.println("Hello on STDERR\n");
 }
}
  
