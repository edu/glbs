/*---------------
  parameter-demo made with frexp
  (c) H.Buchmann FHNW 2007
  $Id$
  ---------------*/
#include <stdio.h>
#include <math.h>

int main(unsigned argc,char** args)
{
 double x=10.0;
 int exp=0;  /* will be overwritten in frexp*/
 double mantissa=frexp(x,&exp);
                       /*^-------------- address of */
 printf("%f=%f*2^%d\n",x,mantissa,exp); /* 10=0.625*2^4 */
}

