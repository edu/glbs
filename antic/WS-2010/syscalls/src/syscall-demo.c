/*------------------ 
  syscall-demo
  (c) H.Buchmann FHNW 2009
  $Id$
  ------------------*/
#include <stdio.h>         /* standard input/output */
#include <sys/types.h>
#include <unistd.h>
                               /* see java packages */
int main(unsigned argc,char** args)
{
 printf("Hello World %d %x\n",getpid(),getpid()); 
                           /* syscall printf syscall */
 return 0;                                    /* ok */
}
