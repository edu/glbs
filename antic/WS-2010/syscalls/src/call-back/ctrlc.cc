//----------------------------
//CtrlC
//(c) H.Buchmann FHNW 2006
//$Id$
//----------------------------
#include <iostream>
using namespace std;

//#include <unistd.h>
#include <signal.h>
//------------------------------------- CtrlC 
// the service class
class CtrlC
{
 public:
//------------------------------------- CtrlC::Listener
//must be subclassed by user
  class Listener
  {
   public:
    virtual ~Listener();
    virtual bool onCtrlC()=0; 
      //returns true iff process shall stop
  };
  
           CtrlC();
  virtual ~CtrlC();
  static Listener* register_(Listener* li);
    //returns the old one
  
 private:
  static CtrlC ctrlc;
  Listener* li;
  static void signalHandler(int signal);
};

CtrlC CtrlC::ctrlc;

CtrlC::CtrlC()
{
 ::signal(SIGINT,signalHandler);
}

CtrlC::~CtrlC()
{
}

CtrlC::Listener::~Listener()
{
}

CtrlC::Listener* CtrlC::register_(Listener* li)
{
 Listener* old=ctrlc.li;
 ctrlc.li=li;
 return old;
}

void CtrlC::signalHandler(int signal)
{
 if (ctrlc.li) 
    {
     if (ctrlc.li->onCtrlC()) ::exit(0);
    }
}

//------------------------------------- Demo
class MyListener:public CtrlC::Listener
{
 private:
 public:
  bool onCtrlC();
};

bool MyListener::onCtrlC()
{
 cerr<<"CtrlC pressed\n";
 return false; //dont stop
}


int main(unsigned argc,char* args[])
{
 MyListener li;
 CtrlC::register_(&li);
 while(true) ::pause();
 return 0;
}

