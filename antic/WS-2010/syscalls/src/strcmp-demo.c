/*-------------
  strcmp-demo
  (c) H.Buchmann FHNW 2007
  $Id$
  -------------*/
#include <stdio.h>  /* printf & co */
#include <string.h>      /* strcmp */

int main(unsigned argc,char** args)
{
 char s1[]="BAAAA";
 char s2[]="AB";
 int res=strcmp(s1,s2);
 printf("res=%d\n",res);
 return 0;
}
  
