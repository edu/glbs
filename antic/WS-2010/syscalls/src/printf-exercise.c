/*---------------------
  printf-exercise
  (c) H.Buchmann FHNW 2008
  $Id$
-----------------------*/
#include <stdio.h>
/*
 expected output
         1
        11
       121
      1331
     14641
    161051
   1771561
  19487171
 214358881
2357947691
*/
int main(int argc,char** args)
{
 double v=1;
 for(unsigned i=0;i<10;++i)
 {
  printf("%20.0f\n",v);
  v*=11.0;
 }
 return 0;
}
