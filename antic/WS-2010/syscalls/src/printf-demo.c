/*---------------
  printf-demo
  (c) H.Buchmann FHNW 2008
  $Id$
-----------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 char ch='A';
 int  val=-4;
 char s[]="Hello World";
 printf(
  "ch=%c val=%d s=%s\n",
      ch,   val,   s
       );
}
  
