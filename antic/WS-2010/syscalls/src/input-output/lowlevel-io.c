/*----------------------
  lowlevel-io
  (c) H.Buchmann FHNW 2009
  $Id$
------------------------*/
#include <stdio.h>
/* for open */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
/* for close read write */
#include <unistd.h>

int main(int argc,char** args)
{
 { /* block for write */
  int dst=open("xxx",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR);
  if (dst<0)
     {
      perror("open write");
      return 1;
     }
  unsigned data[]={100,200,300,400,500};
  write(dst,data,sizeof(data));
  close(dst);
 }
 { /* block for read */
  int src=open("xxx",O_RDONLY);
  if (src<0)
     {
      perror("open read");
      return 1;
     }
  unsigned data[100];
  int siz=read(src,data,sizeof(data));
  if (siz<0)
     {
      perror("read");
      return 1;
     }
  /* siz>=0 */
  close(src);
  for(unsigned i=0;i<siz/sizeof(unsigned);++i)
  {
   printf("%d\n",data[i]);
  } 
 }
 return 0;
}

