/*--------------------
 scanf.c
 (c) H.Buchmann FHNW 2009
 $Id$
----------------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 unsigned val0=0;
 unsigned val1=1;
 int res=scanf("%d %d",&val0,&val1);
 printf("res=%d\nval0=%d val1=%d\n",res,val0,val1);
 return 0;
}
  
