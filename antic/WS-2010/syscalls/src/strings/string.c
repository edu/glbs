/*-----------------
 string
 (c) H.Buchmann FHNW 2009
 $Id$
 ------------------*/
#include <string.h>
#include <stdio.h>
int main(int argc,char** args)
{
 char s[]="Hello World";
 unsigned len=strlen(s);
 printf("len=%d\n",len);
 
 char dst[100];
 strncpy(dst,s,100);
 printf("dst=%s\n",dst); 
 return 0;
}
   
