//----------------------
//syscall example C Version
//(c) H.Buchmann FHNW 2006
//$Id$
//----------------------
#include <stdio.h>  //for C in/output
#include <string.h> //stuff for elementary strings

int main(unsigned argc,char* args[])
{
 char s[]="0123456789";
 printf("strlen('%s')=%d\n",s,strlen(s));
 return 0; 
}
