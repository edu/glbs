#--------------------
#make-boot.sh
#(c) H.Buchmann FHNW 2011
#$Id$
#calling sh make-boot.sh srcFile
#--------------------
SRC=${1}
SRC0=${SRC%.*} #remove extension without extension
gcc -c ${1} -o ${SRC0}.o
objcopy --output-target binary ${SRC0}.o ${SRC0}.bin
