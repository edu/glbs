//---------------------------
//Echo
//(c) H.Buchmann FHNW 2010
//$Id$
//---------------------------
import java.net.Socket;
import java.net.ServerSocket;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.LineNumberReader;
import java.io.InputStreamReader;
class Echo
{
 static final int PORT=31415;
//Echo server [port]  
//Echo client ip [port] msg

 static void copy(InputStream in,OutputStream out) throws Exception
 { 
  while(true)
  {
   int b=in.read();
   if (b<0) break;
   out.write(b);
  }
  out.flush(); //for sure
 }
 
 static class Connection
 {
  protected Socket     sock;
  protected InputStream  in;
  protected OutputStream out;

  Connection(Socket sock) throws Exception
  {
   this.sock=sock;
   this.in=sock.getInputStream();
   this.out=sock.getOutputStream();
  }
 }
 
 static class Client extends Connection
 {
  private Client(String ip,int port,String msg) throws Exception
  {
   super(new Socket(ip,port));
   PrintWriter out=new PrintWriter(super.out);
   out.println(msg);
   out.flush();
   LineNumberReader in=new LineNumberReader(new InputStreamReader(super.in));
   System.out.println(in.readLine());
   sock.close();
  }
 }
 
 static class Server
 {
  static class Connection extends Echo.Connection
                          implements Runnable
  {
   private Thread th=new Thread(this);
   Connection(Socket sock) throws Exception
   {
    super(sock);
    System.err.println(sock);
    th.start();
   }
  
   public void run()
   {
    try
    {
     while(true)
     {
      int b=in.read();
      if (b<0) break;
      out.write(b);
     }
     out.flush();
     sock.close();
    }
    catch(Exception ex)
    {
     ex.printStackTrace();
    }
   }
  }
  
  private Server(int port) throws Exception
  { 
   ServerSocket service=new ServerSocket(port);
   System.err.println("listening on port "+port);
   while(true)
   {
    new Connection(service.accept());
   }
  }
 }
 
 public static void main(String args[]) throws Exception
 {
  switch(args.length)
  {
   case 1:
    if (!args[0].equals("server")) break;
    new Server(PORT);
   return;

   case 2: 
    if (!args[0].equals("server")) break;
    new Server(Integer.parseInt(args[1]));
   return;
   
   case 3:
    if (!args[0].equals("client")) break;
    new Client(args[1],PORT,args[2]);
   return;

   case 4:
    if (!args[0].equals("client")) break;
    new Client(args[1],Integer.parseInt(args[1]),args[2]);
   return;
   
  }
  System.err.println("usage Echo server [port]\n"+
                     "      Echo client io [port] msg");  
 }
}
