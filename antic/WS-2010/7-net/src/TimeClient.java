//----------------------
//TimeClient
//(c) H.Buchmann FHNW 2010
//$Id$
//see RFC 868
//----------------------
import java.io.DataInputStream;
import java.net.Socket;
import java.util.Date;

class TimeClient
{
 final static int PORT=37;
 private TimeClient(String ip) throws Exception
 {
  Socket sock=new Socket(ip,PORT);
  DataInputStream in=new DataInputStream(sock.getInputStream());
  int val=in.readInt();
  sock.close();
  long v=0x10000000l+(0xffffffffl&val);
 }

 public static void main(String args[]) throws Exception
 {
  new TimeClient(args[0]);
 }
}
