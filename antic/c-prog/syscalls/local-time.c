/*----------------
local-time
(c) H.Buchmann FHNW 2007
$Id$
------------------*/
#include <stdio.h>
#include <time.h>

int main(unsigned argc,char** args)
{
 time_t ti;  /* times in sec since 1.1.1970 */
 time(&ti);
 printf("ti=%u\n",ti);
 struct tm loc_time; /* definition */
 printf("tm_mday=%d\n",loc_time.tm_mday); /* formatted time */
 localtime_r(&ti,&loc_time);
 printf("%d-%d-%d\n",
        loc_time.tm_mday,
	1+loc_time.tm_mon,
	1900+loc_time.tm_year);
 return 0;
}
