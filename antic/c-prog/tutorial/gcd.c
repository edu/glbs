/*
-------------------------
gcd greatest common divisor
(c) H.Buchmann FHNW 2006
$Id$
------------------------- 
*/
#include <stdio.h>

unsigned gcd(unsigned x,unsigned y)
{
 while(y>0)
 {
  unsigned z=x%y;
  x=y;
  y=z;
 }
 return x;
}

int main(unsigned argc,char** args)
{
 unsigned x=987;
 unsigned y=1597;
 printf("%d,%d\n",x,y);
 printf("gcd(%d,%d)=%d\n",x,y,gcd(x,y));
}
