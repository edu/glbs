/*
--------------------------
exercise-1
(c) H.Buchmann FHNW 2006
$Id$
--------------------------
*/
#include <stdio.h>
int main(unsigned argc,char** args)
{
 unsigned  val0=5;
 unsigned* p0=&val0;
 
 cerr<<"val0= "<<val0<<" p0= "<<p0<<" *p0= "<<*p0<<"\n";

 *p0=6;

 cerr<<"val0= "<<val0<<" p0= "<<p0<<" *p0= "<<*p0<<"\n";
 
 unsigned val1=*p0;
 unsigned* p1=&val1;

 cerr<<"val1= "<<val1<<" p1= "<<p1<<" *p1= "<<*p1<<"\n";
 
 *p1=*p0;
 cerr<<"val0= "<<val0<<" p0= "<<p0<<" *p0= "<<*p0<<"\n"
       "val1= "<<val1<<" p1= "<<p1<<" *p1= "<<*p1<<"\n";
 p1=p0;
 cerr<<"val0= "<<val0<<" p0= "<<p0<<" *p0= "<<*p0<<"\n"
       "val1= "<<val1<<" p1= "<<p1<<" *p1= "<<*p1<<"\n";

 
 return 0;
}
