/*----------------
function-call
(c) H.Buchmann FHNW 2006
------------------*/
#include <stdio.h>

void aFunc1(unsigned val)
{
 val=val+1;
}

void aFunc2(unsigned* val)
          /* |----------------- pointer */
{
   *val=*val+1;
/* |----|---------- deref */
}

int main(unsigned argc,char** args)
{
 unsigned v0=10;
 printf("before v0= %u\n",v0);
 aFunc1(v0);
 printf("after aFunc1 v0= %u\n",v0);
 /* no change in v0 */
 aFunc2(&v0);
     /* |------------ address of */
 printf("after aFunc2 v0= %u\n",v0);
 /* change in v0 */
 return 0;
 
}
