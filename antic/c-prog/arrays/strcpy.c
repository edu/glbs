/*--------------------------------
  strcpy
  semantics see manual page of strcpy
  (c) H.Buchmann FHNW 2006
  $Id$
----------------------------------*/
#include <stdio.h>

/*assuming there is enough space in dst */
/*assuming no overlapping dst+strlen(dst) 
                           src+strlen(src) */
/* TODO
 handle overlapping 
 
  |-----------------------|
  dst                     dst+len
        |-----------------------|
        src                     src+len 
 or
 
  |-----------------------|
  src                     src+len
        |-----------------------|
        dst                     dst+len 
 
*/
void strcpy(char* dst,char* src)
{
 unsigned i=0; /* index in dst/src */
 while(1)
 {
  char ch=src[i]; /* get src like an array */
  dst[i]=ch;      /* put dst like an array */
  if (ch=='\0') break; 
  ++i;            /* next */
 }
}

void strncpy(char* dst,char* src,unsigned cap)
                                     /* cap capacity of dst */
{
 unsigned i=0;
 while(1)
 {
  if (i==cap) return; /* dst too small */
                      /* dont close with a terminating zero */
  char ch=src[i];
  dst[i]=ch;
  if (ch=='\0') break;
  ++i;             
 }
}

int main(unsigned argc,char** args)
{
 char src[]  ="1234567"; /* the source */
 char dst[8];            /* the destination not initialized */
                         /* too short for src */

 strncpy(dst,src,8);
 printf("src= %s\n"
        "dst= %s\n",src,dst); 
 return 0;
}
