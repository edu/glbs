#include <unistd.h>
#include <signal.h>
#include <iostream>
using namespace std;

void handler(int sig)
{
 cerr<<"Signal "<<sig<<"\n";
}

int main(unsigned argc,char* args[])
{
 ::signal(64,handler); 
 ::signal(63,handler);
 cerr<<"pid = "<<::getpid()<<"\n";
 while(true) pause(); //waits for a signal
 return 0;
}
