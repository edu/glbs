/*--------------
 scanf-demo
 (c) H.Buchmann FHNW 2008
 $Id$
----------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 unsigned val0=0;
 unsigned val1=0;
 scanf("%d %d",&val0,&val1);
           /*  ^-----^---------- address of */
 printf("val0=%d val1=%d\n",val0,val1);
 return 0;
}
  
