//---------------
//CallBackDemo
//(c) H.Buchmann FHNW 2008
//$Id$
//---------------
import java.awt.Frame;
import java.awt.Panel;
import java.awt.Button;
import java.awt.event.ActionListener;  // interface 
import java.awt.event.ActionEvent;

class CallBackDemo implements ActionListener
{
 private Frame  frame =new Frame("CallBackDemo");
 private Panel  panel =new Panel();       //default layout manager
 private Button button=new Button("clickMe"); 
 CallBackDemo() //constructor
 {
  System.err.println("The CallBackDemo");
  button.addActionListener(this); //this implements ActionListener
  panel.add(button);
  frame.add(panel);
  frame.setVisible(true);
 }

//implementation ActionListener
//callback handler
 public void actionPerformed(ActionEvent e)
 {
  System.err.println("actionPerformed  "+e);
 }
  
 public static void main(String args[])
 {
  new CallBackDemo();
 }
}
