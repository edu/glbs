/*
----------------------------
async-input
(c) H.Buchmann FHNW 2006
$Id$
----------------------------
*/
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>

void handler(int sig)
{
 printf("x");
}

int main(unsigned argc,char** args)
{
 signal(SIGIO,handler);
 int id=open("/dev/tty",O_RDONLY|O_ASYNC); 
 int err=fcntl(id,F_SETOWN,getpid());
 if (err<0)
    {
     perror("fcntl");
     exit(1);
    }
 while(1)
 {
#if 0
  char ch;

  int cod=read(id,&ch,sizeof(ch)); //waits to end of line
  if (cod<0) 
     {
      perror("read");
      exit(1);
     } 
  printf("%c : %d\n",ch,(int)ch);
#endif 
  pause();  
 }
 return 0;
}
