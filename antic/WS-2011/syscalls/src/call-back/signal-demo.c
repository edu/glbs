/*------------------
  signal-demo
  (c) H.Buchmann FHNW 2007
  $Id$
  ------------------*/
#include <signal.h> /* for signal */
#include <unistd.h> /* for pause */
#include <stdio.h>

/* call back */
void onSignal1(int sigN)      /* will be eventually called by os */ 
{
 printf("onSignal--1 %d\n",sigN);
}

void onSignal2(int sigN)      /* will be eventually called by os */ 
{
 printf("onSignal--2 %d\n",sigN);
}

int main(unsigned argc,char** args)
{
 printf("pid=%d\n",getpid());   /* displays process id */
 signal(SIGRTMAX  ,onSignal1);             /* register */
 signal(SIGRTMAX-1,onSignal1);             /* register */
 signal(SIGTERM   ,onSignal2);
 signal(SIGINT    ,onSignal1); 
 while(1)
 {
  pause();
 }
  
 return 0;
}
  
