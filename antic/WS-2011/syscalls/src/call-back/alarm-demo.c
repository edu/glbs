/*-----------------
  alarm-demo simple call back
  (c) H.Buchmann FHNW 2007
  $Id$
  -----------------*/
#include <stdio.h>  
#include <unistd.h>
#include <signal.h>  /* for the singnal names */

extern void tack(int s); 
void tick(int sig) /* my handler */
{
 printf("tick\n");
 signal(SIGALRM,tack);
 alarm(1);
}

void tack(int sig) /* my handler */
{
 printf("tack\n");
 signal(SIGALRM,tick);
 alarm(1);
}
  
int main(unsigned argc,char** args)
{
 signal(SIGALRM,tick);
 alarm(1);
 char ch;
 read(STDIN_FILENO,&ch,1);
 return 0;
}
