/*
--------------------------
 time-demo
 (c) H.Buchmann FHNW 2006
 $Id$
--------------------------
*/
#include <time.h>     /* time stuff */
#include <stdio.h>    /* standard input/output */

int main(unsigned argc,char* args[])
{
/* time_t ti=time(0); */
               /* ^----------- null pointer  */
 time_t ti;  /* declaration */
 time(&ti);
   /* ^------ address of */
 printf("ti= %d\n",ti); 

 struct tm* time;
 time=localtime(&ti);
             /* ^---------- address of*/
 printf("%d:%d:%d\n",time->tm_hour,time->tm_min,time->tm_sec);
 printf("%s\n",asctime(time));     
 return 0;   /* ok */
}
