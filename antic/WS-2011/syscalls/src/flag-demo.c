/*
------------------
flag-demo
------------------
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

int main(unsigned argc,char** args)
{
 printf("O_WRONLY %x O_APPEND %x\n",O_WRONLY,O_APPEND);
 int id=open("xxx.txt",O_WRONLY|O_APPEND);
 if (id<0)
    {
     perror("open");
     exit(1);
    }
 char txt[]="1234567890";
 write(id,txt,10);
 close(id);
 return 0;
}
