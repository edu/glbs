//----------------------
//syscall example C++ Version
//(c) H.Buchmann FHNW 2006
//$Id$
//----------------------
#include <iostream> //for C++ in/output
using namespace std;

#include <string.h> //stuff for elementary strings

int main(unsigned argc,char* args[])
{
 char s[]="0123456789";
 cerr<<"strlen('"<<s<<"')="<<::strlen(s)<<"\n"; 
 return 0; 
}
