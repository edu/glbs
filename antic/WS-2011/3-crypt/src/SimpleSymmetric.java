//---------------------------
//SimpleSymmetric
//(c) H.Buchmann FHNW 2011
//$Id$
//---------------------------
class SimpleSymmetric
{
 public SimpleSymmetric(int key) throws Exception
 {
  while(true)
  {
   int ch=System.in.read();
   if (ch<0) break;
   System.out.write(ch^key); //so coder/encoder are the same
  }
  System.out.close();
 }
 
 private static void usage()
 {
  System.err.println("usage: SimpleSymmetric encode|decode key");
  System.exit(1);
 }
 
 public static void main(String args[]) throws Exception 
 {
  if (args.length!=2) usage();
  if (args[0].equals("encode")) 
     {
      new SimpleSymmetric(Integer.decode(args[1]));  
      return;
     }
  if (args[0].equals("decode")) 
     {
      new SimpleSymmetric(Integer.decode(args[1]));  
      return;
     }
  usage();
 }

}
