#----------------------
#rsa-encrypt.sh
#(c) H.Buchmann FHNW 2010
#$Id$
#----------------------
#msg 64 byte 512 bits
openssl rsautl -raw -inkey rsa.private -decrypt
