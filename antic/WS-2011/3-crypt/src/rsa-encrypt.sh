#----------------------
#rsa-encrypt.sh
#(c) H.Buchmann FHNW 2010
#$Id$
#----------------------
openssl rsautl -raw -inkey rsa.public -pubin -encrypt
