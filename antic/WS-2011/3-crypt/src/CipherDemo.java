//-------------------------
//CipherDemo
//(c) H.Buchmann FHNW 2010
//$Id$
//-------------------------
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import java.security.Key;
import javax.crypto.KeyGenerator;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

class CipherDemo
{
 private Cipher cipher=Cipher.getInstance("DES");
 
 private CipherDemo() throws Exception
 {
 }
 
 private void key() throws Exception
 {
  ObjectOutputStream out=new ObjectOutputStream(System.out);
  out.writeObject(KeyGenerator.getInstance("DES").generateKey());
  out.close();
 }
 
 private Key getKey(String keyFile) throws Exception
 {
  ObjectInputStream in=new ObjectInputStream(
                               new FileInputStream(keyFile)
                                            );
  Key k=(Key)in.readObject();
  in.close();
  return k;
 }
 
 private void copy(InputStream from,
                   OutputStream to) throws Exception
 {
  while(true)
  {
   int ch=from.read();
   if (ch<0) break;
   to.write(ch);
  }
  from.close();
  to.close();
 }
 
 private void encode(String keyFile) throws Exception
 {
  cipher.init(Cipher.ENCRYPT_MODE,
              getKey(keyFile));
  CipherOutputStream out=new CipherOutputStream(System.out,cipher);
  copy(System.in,out);
 }

 private void decode(String keyFile) throws Exception
 {
  cipher.init(Cipher.DECRYPT_MODE,
              getKey(keyFile));
  CipherInputStream in=new CipherInputStream(System.in,cipher);
  copy(in,System.out);
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args[0].equals("key"))
     {
      new CipherDemo().key();
      return;
     }
  if (args[0].equals("enc"))
     {
      new CipherDemo().encode(args[1]);
      return;
     }
  if (args[0].equals("dec"))
     {
      new CipherDemo().decode(args[1]);
      return;
     }
  System.err.println("usage: CipherDemo key | enc key | dec key\n"+
                     " input: stdin output: stdout");
 }
}
