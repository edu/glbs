//----------------------------
//term-demo
//(c) H.Buchmann FHNW 2006
//$Id$
//----------------------------
#include <iostream>
using namespace std;

#include "sos.h"

class Demo:public Term::Listener,
           public Timer::Listener
{
 private:
  Term term;
  Timer timer;
  int x,y;
  void onKey(char ch);
  void onEnter();
  void onUp();
  void onDown();
  void onLeft();
  void onRight();

  void onTick();
  
 public:
   Demo(const char device[]);
  ~Demo();
};

Demo::Demo(const char device[])
:term(device,this)
,timer(Timer::SECOND/2,this)
,x(0)
,y(0)
{
// term.clear();
 SOS::start(&term,&timer);
}

Demo::~Demo()
{
}


void Demo::onKey(char ch)
{
 term<<"key= '"<<ch<<"' "<<(unsigned)ch<<"\n"; 
}

void Demo::onEnter()
{
 term<<"enter\n";
}

void Demo::onUp()
{
 term<<"up\n";
}

void Demo::onDown()
{
 term<<"down\n";
}

void Demo::onLeft()
{
 term<<"left\n";
}

void Demo::onRight()
{
 term<<"right\n";
}

void Demo::onTick()
{
 term<<"tick\n";
// term(x++,y++)<<"x";
}

int main(unsigned argc,char* args[])
{
 Demo demo("/dev/tty");
 return 0;
}
