#ifndef SOS_H
#define SOS_H
//----------------------------
// SOS Simple Operating System
// example of event driven programming
// (c) H.Buchmann FHNW 2006
//$Id$
//----------------------------
#include <termios.h>
#include <sys/time.h>
#include <iostream>
#include "thread.h"

//--------------------------------------- Term
class Term:public Runnable
{
 public:
  class Listener
  {
   public:
    virtual ~Listener();
    virtual void onKey(char ch){}
    virtual void onEnter(){}
    virtual void onUp(){}
    virtual void onDown(){}
    virtual void onLeft(){}
    virtual void onRight(){}
  };

 private:
  Term(const Term&);
  friend class SOS;
  static const char ESC[];
  typedef void (Listener::*Action)();
  
  struct ToAction
  {
   const char* code;
   Action act;
  };

  static const ToAction Action_[];
  
  Thread th;
  int id;       //of device
  termios spec; //the ols one
  Listener* li;
  unsigned escapeN; //the size
  char escape[80];
  void run();
  void emptyEscape();
  void onChar(char ch);
    
 public:

  template<typename T>
  friend Term& operator<<(Term& term,const T t)
  {
   std::cerr<<t;
   return term;
  }

           Term(const char device[],Listener* li);
  virtual ~Term();
  
  Term& clear();
//cursor setting
  Term& operator()(unsigned x,unsigned y);
  Term& up();
  Term& down();
  Term& left();
  Term& right();
};

//--------------------------------------- Timer
class Timer:public Runnable
{
 public:
  static const unsigned SECOND=1000000; 
              //!< useful for specifying long time intervals
    
  class Listener
  {
   public:
    virtual ~Listener();
    virtual void onTick(){};
  };

 private:
  Timer(const Timer&);
  friend class SOS;
  Thread th;
  unsigned delta;
  Listener* li;
  void run();

 public: 
           Timer(unsigned delta_us,Listener* li);
  virtual ~Timer();
};

class SOS 
{
 public:
  static void start(Term* term,Timer* timer);
};
#endif

