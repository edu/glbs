//------------------------
//thread for c++ see java threads
//(c) H.Buchmann FHNW 2006
//$Id$
//------------------------
#include "thread.h"
#include <iostream>

//------------------------------- Mutex
Mutex::Mutex()
{
 ::pthread_mutex_init(&mutex,0);
}

Mutex::~Mutex()
{
 ::pthread_mutex_destroy(&mutex);
}

void Mutex::lock()
{
 ::pthread_mutex_lock(&mutex);
}

void Mutex::unlock()
{
 ::pthread_mutex_unlock(&mutex);
}

//------------------------------- Runnable
Runnable::~Runnable()
{
}

Thread::Thread(Runnable* run)
:run(run)
{
}

//------------------------------- Thread
Thread::~Thread()
{
}

void Thread::start()
{
 ::pthread_create(&th,0,start,this);
}

void* Thread::start(void* thread)
{
 ((Thread*)thread)->run->run();
 return 0;
}

