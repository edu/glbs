//---------------------
//Histogram
//(c) H.Buchmann FHNW 2008
//$Id$
//TODO better configuration properties
//---------------------
//properties
//bin:   the number of bins
//delta: the width of a bin
class Histogram
{
 private int tooBig=0;
 private int binN=read("bins");
 private int bins[]=new int[binN];
 private int width=read("width");
 
 static private void usage()
 {
  System.err.println("properties bins: number of bins\n"+
                     "           width: of the bins");
  System.exit(1);
 }
 
 static private int read(String prop)
 {
  try
  {
//   System.err.println(prop+"="+System.getProperty(prop));
   
   return Integer.parseInt(System.getProperty(prop));
  }
  catch(NumberFormatException ex)
  {
   usage();
   return 0;
  }
 }
  
 private void put(String l) throws Exception
 {
  int i=l.indexOf(' '); //first blank
  if (i<0) throw new Exception(l);
  int n=Integer.parseInt(l.substring(0,i));
  int idx=n/width;
  if (idx<binN) ++bins[idx];
     else       ++tooBig;
 }
 
 private void show()
 {
  int x=0;
  for(int i=0;i<bins.length;++i)
  {
   System.out.println(x+"\t"+i);
   x+=width;
  }
  System.out.println(x+"\t"+tooBig); 
 }
 
 private Histogram() throws Exception
 {
  java.io.LineNumberReader src=new java.io.LineNumberReader(
    new java.io.InputStreamReader(System.in)
   );
  while(true)
  {
   String l=src.readLine();
   if (l==null) break;
   put(l);
  }
  show();
 }
 
 public static void main(String args[])throws Exception
 {
  new Histogram();
 }
}
