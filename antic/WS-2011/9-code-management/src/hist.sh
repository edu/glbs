#---------------
#hist.sh
#(c) H.Buchmann FHNW 2009
#$Id$
#---------------
if [ -z ${1} ]
  then echo "usage ${0} dir"
       exit 1
fi
BINS=40
WIDTH=50
sh lines.sh ${1} | java -Dbins=${BINS} -Dwidth=${WIDTH} Histogram

