//--------------------------
//RegularPolygon.java
//(c) H.Buchmann FHNW 2010
//$Id$
//calling java RegularPolygon numberOfVertices
// n num
//--------------------------
class RegularPolygon
{
//TODO handle negative n
 private RegularPolygon(int n)
 {
  double alfa=2*Math.PI/n;
  double sin=Math.sin(alfa);
  double cos=Math.cos(alfa);
  double x=1;
  double y=0;
  while(n>0)
  {
   System.out.println(x+"\t"+y);
   double x1=cos*x-sin*y;
   double y1=sin*x+cos*y;
   x=x1;
   y=y1;
   --n;
  }
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args.length!=1) 
     {
      throw new Exception("usage: RegularPolygon numberOfVertices");
             //output on cerr
     }
  new RegularPolygon(Integer.parseInt(args[0]));
 }
}

