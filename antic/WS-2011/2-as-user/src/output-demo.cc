//-----------------------
//  output-demo
//  (c) H.Buchmann FHNW 2009
//  $Id$
//-------------------------*
#include <iostream>
using namespace std;

int main(int argc,char** args)
{
 cout<<"Hello on STDOUT\n";
 cerr<<"Hello on STDERR\n";
 return 0;
}
  
