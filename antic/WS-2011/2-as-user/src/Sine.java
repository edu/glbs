//---------------------------
//Sine
//(c) H.Buchmann FHNW 2010
//$Id$
//---------------------------
import java.io.DataOutputStream;
//DataOutputStream: BIG endian
class Sine
{
 static final double FS=44000; //Hz
 
 static private int scale(double v) //-1<=v<=1
 {
  return (int)(Integer.MAX_VALUE*v);
 }
 
 Sine(double f,            //Hz
      double duration      //Sec 
     ) throws Exception
 {
  DataOutputStream out=new DataOutputStream(System.out);
  double alfa=2*Math.PI*f/FS;
  double cos=Math.cos(alfa);
  double sin=Math.sin(alfa);
  double x=1.0;
  double y=0.0;
  double dt=1.0/FS;  
  for(double t=0;t<duration;t+=dt)
  {
   out.writeInt(scale(y));
   double x1=cos*x-sin*y;
   double y1=sin*x+cos*y;
   x=x1;
   y=y1;
  }
  out.close();
 }

//args[0] f
//args[1] dur 
 public static void main(String args[]) throws Exception
 {
  new Sine(Double.parseDouble(args[0]),
           Double.parseDouble(args[1]));
 }
}
