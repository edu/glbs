//---------------------
//ThreadDemo
//(c) H.Buchmann FHNW 2010
//$Id$
//---------------------
class MyThread
{
 private String id;
 
 MyThread(String id)
 {
  this.id=id;
 }
 
 void run()
 {
  while(true)
  {
   System.out.println(id);
  }
 }
}

class ThreadDemo
{
 public static void main(String args[])
 {
  new MyThread("T1");
  new MyThread("T2");
 }
}
