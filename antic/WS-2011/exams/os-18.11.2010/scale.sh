#------------------------
#scale.sh
#(c) H.Buchmann FHNW 2010
#$Id$
#------------------------
#the scale
C=261.63
CIS=277.18
D=293.66
DIS=311.13
E=329.63
F=349.23
FIS=369.99
G=392.00
GIS=415.30
A=440.00
AIS=466.16
H=493.88

#calling 
#play frequency in hertz
#example
#play 1000 plays 1000 Hz sine
function play() 
{
 #assume here some code for real playing
}

echo "pid ${$}" #prints process id
while [ true ]
do
 echo "tick"
 sleep 1s
done


