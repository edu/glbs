//---------------
//CallStack
//(c) H.Buchmann FHNW 2010
//$Id$
//---------------
class CallStack 
{
 private void aFunction() throws Exception
 {
  throw new Exception("an Exception"); //crashes 
 }
 
 CallStack() throws Exception
 {
  aFunction();
 }

 public static void main(String args[]) throws Exception 
 {
  new CallStack();
 }
}
