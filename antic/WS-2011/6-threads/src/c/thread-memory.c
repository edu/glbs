/*-------------------
  thread-memory
  (c) H.Buchmann FHNW 2007
  $Id$
---------------------*/
#include <pthread.h>
#include <stdio.h>

unsigned val=0;          /* global  variable */  
pthread_mutex_t mutex;   /* defined but not initialized */

void* run(void* p) 
{
 unsigned id=*(unsigned*)p; /* copy to local id */
 printf("id= %u\n",id);
 while(1)
 {  /* complicated val++ */
  pthread_mutex_lock(&mutex);
  unsigned l=val; /* local copy */
  l=l+1; 
  if (l!=(val+1)) printf("---- %d val= %u\n",id,l);
  val=l;  /* copy local to global */
  pthread_mutex_unlock(&mutex);
 }
}


int main(unsigned argc,char** args)
{
 pthread_mutex_init(&mutex, /* the mutex */
                    0);     /* default attributes */
 pthread_t th0;
 unsigned id0=0;
 pthread_create(&th0,  /* the thread */
                0,     /* the attributes */
		run,   /* the code */
		&id0);
 pthread_t th1;
 unsigned id1=1;
 pthread_create(&th1,  /* the thread */
                0,     /* the attributes */
		run,   /* the code */
		&id1);
 getc(stdin); /* wait */
 printf("val=%u\n",val);
 return 0;
}
