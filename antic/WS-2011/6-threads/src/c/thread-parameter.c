/*--------------------
 thread-parameter
 (c) H.Buchmann FHNW 2008
 $Id$
 compile/link with -std=c99 -lpthread
----------------------*/
#include <stdio.h>
#include <pthread.h>

typedef 
struct {
 unsigned p1;
 unsigned p2;
} Parameter;


void* myRun(void* p) /* return value parameter see: pthread_create */
{
 Parameter lp=*(Parameter*)p;          /* see unsigned id0 in main */
                                        /* id copy of id0 in main */       
 while(1)
 {
  printf("myRun %d \n",lp.p1);
  for(unsigned i=0;i<(1<<20);++i)       /* 1<<20 <-> 2^20 */
  {
  }
 }
 return 0;
}


int main(int argc,char** args)
{
 pthread_t th0;
 Parameter p0={2,3};
 unsigned id0=1;         /* identification for thread 0 */
 pthread_create(&th0,                     /* the handle */
                0,                /* default attributes */
                myRun,                /* the 'Runnable' */ 
                &p0);      /*address of id0 (unsigned*) */
 pthread_t th1;
 Parameter p1={5,6};
 pthread_create(&th1, 
                0,
		myRun,    /* the same 'Runnable' like in th0 */
		&p1);       
 int ch=getc(stdin);		
 return 0;
}
