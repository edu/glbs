/*----------------
  common-access
  (c) H.Buchmann FHNW 2007
  $Id$
 -----------------*/
#include <stdio.h>
#include <pthread.h>

pthread_mutex_t theMutex;     /* defined but not initalized */
unsigned val=0;  /* global variable accessible by everybody */

void* doIt(void* p)
{
 unsigned id=*(unsigned*)p;                 /* copy into id */
 printf("id=%d\n",id);
 unsigned cnt=0;                           /* local counter */
 while(1)
 {
  ++cnt;
  pthread_mutex_lock(&theMutex);  /*<---------------- lock  */
  unsigned l=val;                    /* l local copy of val|*/
  l=l+1;
  if (l!=(val+1)) {printf("** %d\n",id);}
  val=l;                        /* save again in global val */
  pthread_mutex_unlock(&theMutex); /*<-------------- unlock */
  if (cnt==(1<<20))
     {
      printf("<%d>\n",id);
      cnt=0;
     }
 }
}

int main(unsigned argc,char** args)
{
 pthread_mutex_init(&theMutex,0);
 pthread_t th0;
 unsigned id0=0;
 pthread_create(&th0,
                0,
		doIt,
		&id0);

 pthread_t th1;
 unsigned id1=1;
 pthread_create(&th1,
                0,
		doIt,
		&id1);
 
 getc(stdin); /* wait */
}
 
