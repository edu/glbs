//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2008
//$Id$
//----------------
class Pool
{
 public int v=0;
}

class MyRun implements Runnable 
{
 private Pool pool;  //reference to Pool 
 private Thread  th=new Thread(this);
 private int id;
 MyRun(Pool pool,int id)
 {
//  super(this);
  this.pool=pool;
  this.id=id;
  th.start();     //of thread
 }
 
 public void run()
 {
  int defect=0;
  int cnt=1<<22;
  while(true)
  {
   int l=pool.v;
   ++l;
   while(cnt>0) --cnt; // wait 
   if (l!=(1+pool.v)) System.err.println("-o-"+id+" "+(defect++));
   pool.v=l;
  }
 }
}

class CommonAccess
{
 public static void main(String args[]) throws Exception
 {
  Pool pool =new Pool(); //one instance
  MyRun run0=new MyRun(pool,0);
  MyRun run1=new MyRun(pool,1); 
 }
}
