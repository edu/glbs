struct phtread_t
{
 pthread_attr_t* attr;
 void*           arg;
 void* (*theCode)(void*);
 void*           ret;
};
