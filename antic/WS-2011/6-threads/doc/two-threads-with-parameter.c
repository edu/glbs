void* run(void* p)
{
 unsigned id= *(unsigned*)p;
 printf("init %d\n",id);
 while(1)
 {
  printf("run %d\n",id);
 }
 return 0; //never called
}

int main(unsigned argc,char** args)
{
 pthread_t th1;
 pthread_t th2;
 unsigned id0=0;
 pthread_create(&th1,0,run,&id0);
 unsigned id1=1;
 pthread_create(&th2,0,run,&id1);
 getc(stdin);
 return 0;
}
