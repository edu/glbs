/*
--------------------------
variables
(c) H.Buchmann FHNW 2006
$Id$

--------------------------
*/
#include <stdio.h>

unsigned val=7;

int main(unsigned argc,char** args[])
{
 unsigned v=8;
/* assignment */ 
 printf("v before: %d\n",v);
 v=v+val;      
 printf("v  after: %d\n",v);

/* address of */
 printf("address of val= %p\n"
        "             v= %p\n",&val,&v);
 return 0;
}
