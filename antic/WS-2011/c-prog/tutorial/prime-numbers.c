/*-------------------------
  prime-numbers
  (c) H.Buchmann FHNW 2007
  $Id
  $
--------------------------*/  
#include <stdlib.h>
#include <stdio.h>


/* val>=3 */
unsigned isPrime(unsigned val)
{
 for(unsigned d=3;d<val;d+=2)
 {
  if ((val%d)==0) return 0;
 }
 return 1;
}

void primes(unsigned n) /* n the number of primes */
{
/* handle case n==1 */

 unsigned val=3; /* first prime */
 unsigned z=1;   /* the number of primes so far */
 while(z<n)
 {
  if (isPrime(val))
     {
      printf("%u\n",val);
      z++; 
     }
  val=val+2; /* next candidate */   
 }
}

int main(unsigned argc,char** args)
{
 primes(8);
 return 0;
}
