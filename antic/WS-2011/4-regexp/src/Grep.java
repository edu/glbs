//-------------------
//Grep.java
//(c) H.Buchmann FHNW 2009
//$Id$
//calling
// java Grep regex file+
// at least one file
//-------------------
import java.util.regex.Pattern;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.FileNotFoundException;

class Grep
{
 private Pattern pattern;

 private void grep(String file) throws  Exception
 {
  System.err.println(file);
  try
  {
   LineNumberReader src=new LineNumberReader(
                    new FileReader(file)
			                    );
   while(true)
   {
    String line=src.readLine();
    if (line==null) break;
   }
  }
  catch(FileNotFoundException ex)
  {
   //simply skip non existing file
  }
 }
 
 private Grep(String args[]) throws  Exception
 {
  pattern=Pattern.compile(args[0]);
  for(int i=1;i<args.length;++i)
  {
   grep(args[i]);
  }
 } 

 public static void main(String args[]) throws  Exception
 {
  if(args.length<2)
    {
     System.err.println("usage: Grep regex file+\n"+
                        "   at least one file");
     return;
    }
  new Grep(args);
 }
}
