//-------------
//RegexDemo very simple
//(c) H.Buchmann FHNW 2008
//$Id$
//-------------
import java.util.regex.*;

class RegexDemo
{
 public static void main(String args[]) //regex word
 {
  Pattern p=Pattern.compile(args[0]); //static method
  Matcher m=p.matcher(args[1]);
  System.err.println("matches "+m.matches()); 
 }
}

