#---------------
#arithmetic
#(c) H.Buchmann FHNW 2009
#$Id$
#---------------
X=1
Y=2
echo "X+Y"       #as names
echo "${X}+${Y}" #as values
echo "$((X+Y))"  #as sum
