#------------------
#show-code.sh
#(c) H.Buchmann FHNW 2009
#$Id$
#------------------
#parameter ${1} binary file
#          ${2} offset in file 
objdump --target=binary\
        --architecture=i8086\
	--disassemble-all\
	--start-address=${2}\
	${1}
