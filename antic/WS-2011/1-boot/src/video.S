#----------------------
#http://en.wikipedia.org/wiki/BIOS_interrupt_call
#----------------------
	.code16
loop:	mov $0xe,%ah   #Write Character in TTY Mode
	mov $'2' ,%al  #the character
	mov 0x00 ,%bx  #the color
	int $0x10      #call bios service
	jmp loop       #again and again
end:
.fill 1474560-(end-loop),1,0 #must be floopy:  1474560 Bytes
