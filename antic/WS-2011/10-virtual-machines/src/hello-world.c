//--------------------
//hello-world.c
//(c) H.Buchmann FHNW 2011
//$Id$
//--------------------
#include <stdio.h>

int main(int argc,char** args)
{
 printf("Hello World\n");
 return 0;
}
