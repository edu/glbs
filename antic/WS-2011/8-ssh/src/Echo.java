//--------------------------
//Echo
//(c) H.Buchmann FHNW 2011
//$Id$
//--------------------------
class Echo
{
 public static void main(String args[]) throws Exception
 {
  System.out.println("------ Echo-------");
  while(true)
  {
   int ch=System.in.read();
   if (ch<0) break;
   System.out.write(ch);
  }
 }
}
