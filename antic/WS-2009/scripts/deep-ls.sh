#-----------------
#deep-ls.sh
#(c) H.Buchmann FHNW 2006
#$Id$
#------------------
function listIt()
{
 for f in $(ls $1)
 do
  if [ -d $1/$f ]
  then
   listIt $1/$f
  else
   echo $1/$f
  fi
 done 
}

listIt $1



