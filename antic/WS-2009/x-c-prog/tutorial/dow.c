/*
---------------------
 dow day of week
 (c) H.Buchmann FHNW 2006
 $Id$
---------------------
*/
#include <stdio.h>

unsigned dow(unsigned day,unsigned month,unsigned year)
{
 if (month<3) /* m=1 | m=2 */
    {
     month=month+12;
     year=year-1;
    }
 return ( day+
         2*month+
	 ((3*(month+1))/5)+ /*  dont use (3/5)*(month+1) */
	                    /*     3/5 == 0 */
	 1+
	 year+
	(year/4)-
	(year/100)+
	(year/400)
        )%7;
}


int main(unsigned argc,char** args)
{
 int day=31;
 int month=12;
 int year =1900;
 
 printf("w=%d\n",dow(day,month,year));  /* w=aNumber */
 return 0;
}
