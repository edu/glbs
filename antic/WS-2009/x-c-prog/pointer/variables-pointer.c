/*
--------------------
variables pointer
(c) H.Buchmann FHNW
$Id$
--------------------
*/
#include <stdio.h>

int main(unsigned argc,char** args)
{
   unsigned val=7;
/* |        |   |-------- initial value   
/* |        |------------ name symbol
/* |--------------------- type */
 printf("val= %d\n"
        "address of val %p\n",
	val,             /* as value */
	&val       /* address of val */
      /*|---------------- address of */	
       );
 return 0;
}
