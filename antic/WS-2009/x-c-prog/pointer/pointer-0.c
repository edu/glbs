/*
--------------------------
pointer
(c) H.Buchmann FHNW 2006
$Id$
--------------------------
*/
#include <stdio.h>
int main(unsigned argc,char** args)
{
 unsigned val=7;
 unsigned* valP=&val; /* assignment */
 
 printf("val= %d\n"
        "address of val= %p\n"
	"--------------------\n"
	"valP= %p\n"
	"address of     valP=%p\n"
	"dereference of valP=%d",
	val,
	&val,
	valP,
	&valP,
	*valP);
 return 0;
}
