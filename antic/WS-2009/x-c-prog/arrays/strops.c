/*---------------------
string operations
(c) H.Buchmann FHNW 2006
$Id$
compile with --std=gnu9x option
-----------------------*/
#include <stdio.h>

unsigned strlen(char* s)
{
 if (s==0) return 0;         /* points to null */
 unsigned i=0;
 while(s[++i]!='\0');
 return i;
}

int main(unsigned argc,char** args)
{
 char s[]="122";
 printf("strlen('%s')=%u\n",s,strlen(s));
 return 0;
}

