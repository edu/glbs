/*---------------------------
array-pitfalls
(c) H.Buchmann FHNW 2006
-----------------------------*/
#include <stdio.h>

int main(unsigned argc,char** args)
{
/* reading not in range */
 unsigned arr[5];
 for(int i=-5;i<10;++i)
 {
  printf("%5d %u\n",i,arr[i]);
 }

/* writing not in range very dangerous*/
 unsigned v=100;
 for(int i=-5;i<10;++i)
 {
  printf("writing %d\n",i);
  arr[i]=v;
  --v;
 }
 
 return 0;
}
