 1 class CallStack					       
 2 {							       
 3  private java.io.InputStream src;			       
 4
 5  CallStack(String name) throws Exception		       
 6  {  						       
 7   src=new java.io.FileInputStream(name);		       
 8  }  						       
 9 							       
10  public static void main(String args[]) throws Exception   
11  {  						       
12   if (args.length!=1)				       
13    {						       
14 	System.exit(1); 				       
15    }						       
16   new CallStack(args[0]);				       
17  }  						       
18 }							       
