 1 int pthread_create(pthread_t* th,
 2 		      pthread_attr_t* attr,
 3 		      void* (*theCode)(void*),
 4 		      void* arg)
 5 {
 6  ..
 7  th->attr=attr;	/* ev clone */
 8  th->arg =arg;	/* pointer */
 9  th->theCode=theCode;
10  ..
11  /* prepare to start */
12 }
