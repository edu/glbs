/*
------------------------------
first 
(c) H.Buchmann FHNW 2006
$Id$
------------------------------
*/
#include <pthread.h>
#include <stdio.h>

void* run(void* p)
{
 printf("--- run ---\n");
 return 0;
}

int main(unsigned argc,char** args)
{
 pthread_t th;   /*the handle */
 int cod=pthread_create(&th,0,run,0);
 if (cod<0)
    {
     perror("pthread_create");
     exit(1);
    }
 printf("--- main ---\n");
 getc(stdin); //wait  
 return 0;
}
