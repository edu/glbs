/*
----------------------------
semaphore
(c) H.Buchmann FHNW 2006
$Id$
----------------------------
*/
#include <pthread.h>
#include <semaphore.h>

sem_t semaphore;

void* run1(void* arg)
{
 printf("----------- run1 wait\n");
 sem_wait(&semaphore);
 printf("----------- run1 done\n");
 return 0;
}

void* run2(void* arg)
{
 printf("----------- run2 wait\n");
 sem_wait(&semaphore);
 printf("----------- run2 done\n");
 return 0;
}

int main(unsigned argc,char** args)
{
 sem_init(&semaphore,0,2);
}
