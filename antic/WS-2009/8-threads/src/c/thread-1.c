/*
-----------------------------
thread-1
(c) H.Buchmann FHNW 2006
$Id$
-----------------------------
*/
#include <pthread.h>
#include <stdio.h>


void* run(void* p)
{
 unsigned id= *(unsigned*)p;
 printf("init %d\n",id);
 while(1)
 {
  printf("run %d\n",id);
 }
 return 0; //never called
}

int main(unsigned argc,char** args)
{
 pthread_t th1;
 pthread_t th2;
 unsigned id0=0;
 pthread_create(&th1,0,run,&id0);
 unsigned id1=1;
 pthread_create(&th2,0,run,&id1);

 unsigned id2=2;
 pthread_create(&th2,0,run,&id2);

 unsigned id3=3;
 pthread_create(&th2,0,run,&id3);
 
 getc(stdin);
 return 0;
}

