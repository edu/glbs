/*
-----------------------
producer-consumer
(c) H.Buchmann FHNW 2006
$Id$
-----------------------
*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
 char* name;  
 unsigned id;                        /* the identifcation */
 pthread_t th;                              /* the thread */
} Agent;

void showAgent(Agent* ag)
{
 printf("%s:%u\n",ag->name,ag->id);
}

typedef struct
{
 unsigned producer; /* who made it */
 unsigned val;
} SomeThing;

SomeThing* createSomeThing(unsigned producer,unsigned val)
{
 SomeThing* th=malloc(sizeof(SomeThing)); /* from heap new */
 th->producer=producer;
 th->val=val;
 return th;
}

void deleteSomeThing(SomeThing** sth)
{
 free(*sth);
 *sth=0;
}

void showSomeThing(SomeThing* sth)
{
 printf("producer=%u val=%u\n",sth->producer,sth->val);
}

pthread_mutex_t consM;
pthread_mutex_t prodM;
SomeThing* pool = 0;                            /* pointer */
			  /* pool != 0 someThing available */
			    /*      == 0 noThing available */

/*thread run functions  */
void* produce(void* p)  /* p: points to Agent */
{
 
 return 0;
}

void* consume(void* p)   /* p: points to Agent */
{
 return 0;
}

void createProducer(Agent* ag,unsigned id)
{
 ag->name="producer";
 ag->id=id;
 pthread_create(&ag->th, /* pointer to ag->th */
                0,       /* default attributes */
                produce, /* the code */
		ag
               );
}

void createConsumer(Agent* ag,unsigned id)
{
 ag->name="consumer";
 ag->id=id;
 pthread_create(&ag->th, /* pointer to ag->th */
                0,       /* default attributes */
                consume, /* the code */
		ag
               );
}

int main(unsigned argc,char** args)
{
 pthread_mutex_init(&consM,0);
 pthread_mutex_init(&prodM,0);
 Agent prod0;  /* defined but not initialized */
 createProducer(&prod0,0);
 
 Agent cons0;  /* defined but not initialized */
 createConsumer(&cons0,0);
 
 getc(stdin);  /* wait */
 return 0;
}

