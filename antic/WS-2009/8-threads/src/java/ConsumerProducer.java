//------------------
//ConsumerProducer 
//(c) H.Buchmann FHNW 2007
//$Id$
//------------------

class Something //the thing to be produced/consumed
{
 private String val;
 Something(String val)
 {
  this.val=val;
 }
 
 public String toString()
 {
  return val;
 }
}

class Pool      //for the somethings
{
 private Something theThing; //empty or full

 Pool()
 {
  theThing=null;
 }
 
 synchronized void put(Something sth)
 { 
  try
  {
   while(theThing!=null) 
   {
    wait();
   }
   theThing=sth;
   notify();
  }
  catch(InterruptedException ex)
  {
   ex.printStackTrace();
  }
 }
 
 synchronized Something get()
 {
  try
  {
   while(theThing==null) wait();
   Something sth=theThing;
   theThing=null;
   notify();
   return sth;
  }
  catch(InterruptedException ex)
  {
   ex.printStackTrace();
  }
  return null; //for the compiler
 }
 
}

abstract class Agent implements Runnable
{
 protected String id;
 protected Pool   pool; //reference
 private   Thread th=new Thread(this);
 int       cnt=0;
 protected Agent(String id,Pool pool)
 {
  this.id=id;
  this.pool=pool;
 }
 
 protected void start()
 {
  th.start();
 }
}


class Producer extends Agent
{
 private int id=0;
 Producer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
  start();
 }
 
 //implementation Runnable
 public void run()
 { 
  while(true)
  {
   Something sth=new Something(super.id+":"+id);
   pool.put(sth);
   ++cnt;
   ++id;
  }
 }
}

class Consumer extends Agent
{
 Consumer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
  start();
 }
 
 public void run()
 { 
  while(true)
  {
   Something sth=pool.get();
//   System.err.println(sth);
   ++cnt;
  }
 }
}

//the main class
class  ConsumerProducer
{
 public static void main(String args[]) throws Exception
 {
  Pool pool=new Pool(); //one instance
  Producer p0=new Producer("p0",pool);
  Consumer c0=new Consumer("c0",pool);
//  Consumer c1=new Consumer("c1",pool);
  while(true)
  {
   Thread.sleep(500);
   int pc0=p0.cnt;
   int cc0=c0.cnt;
   int cc1=0; //c1.cnt;
   System.err.println(pc0+":\t"+cc0+"\t"+cc1+"\t:"+(pc0-(cc0+cc1)));
  }
 }
}
