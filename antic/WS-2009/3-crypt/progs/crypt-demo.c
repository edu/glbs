/*---------------------
  crypt-demo
  (c) H.Buchmann FHNW 2008
  $Id$
-----------------------*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <crypt.h>
#include <errno.h>
/*-------------------
 remarks:
  - bit oriented  one bit <--> one char
  - cygwin import crypt 
  - see http://info2html.sourceforge.net/cgi-bin/info2html-demo/info2html?(libc.info.gz)DES%2520Encryption
*/

int main(unsigned argc,char** args)
{
 char key[64];              /* 64 bits */
 for(unsigned i=0;i<64;++i) key[i]=1; 
 setkey(key);
 perror("setkey");

 char blk[64];              /* 64 bits */
 for(unsigned i=0;i<64;++i) blk[i]=1;
 errno=0;
 encrypt(blk,0);  /* encode: original -> code */
 perror("encrypt 0");
 
 for(unsigned i=0;i<64;++i)
 {
  if ((i%8)==0) printf("\n");
  printf("%d ",blk[i]);
 }

 printf("\n"); 
 encrypt(blk,1);
 errno=0;
 perror("encrypt 1"); /* decode code->orignial */
 for(unsigned i=0;i<64;++i)
 {
  if ((i%8)==0) printf("\n");
  printf("%d ",blk[i]);
 }
 printf("\n"); 
}
  
