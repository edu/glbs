#--------------------
#check-hash.sh hash-value file
#(c) H.Buchmann FHNW 2009
#$Id$
#--------------------
if [ ${#} -ne 2 ]
  then echo "usage ${0} hash-value file"
       exit 1
fi

if [ ! -f ${2} ]
   then echo "file '${1}' dont exists"
        exit 1
fi

echo $(openssl dgst -sha1 < ${2})

#string check
#if [ x=y ]
#  then 
#  else 
#fi
