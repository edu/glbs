//-----------------------
//worm 
//a SOS application
//(c) H.Buchmann FHNW 2006
//$Id$
//TODO better checking
//     better terminating of program
//-----------------------
#include "sos.h"

//------------------------------------- Board
class Board
{
 private:
  unsigned wi;
  unsigned he;
  Term& term; //for drawing
  void draw();
  
 public:
           Board(unsigned width,unsigned height,Term& term);
  virtual ~Board();
  void clear();
  Term& statusLine();
};

Board::Board(unsigned width,unsigned height,Term& term)
:wi(width)
,he(height)
,term(term)
{
 draw();
}

Board::~Board()
{
}

void Board::draw()
{
 term.clear();
 term<<'+';
 for(unsigned c=0;c<wi;c++) term<<'-';
 term<<"+\n";
 for(unsigned h=0;h<he;h++)
 {
  term<<'|';
  for(unsigned c=0;c<wi;c++) term<<' ';
  term<<"|\n";  
 }
 term<<'+';
 for(unsigned c=0;c<wi;c++) term<<'-';
 term<<"+\n";
}

void Board::clear()
{
 term(1,he+2);
 for(unsigned i=0;i<40;i++) term<<' ';
}

Term& Board::statusLine()
{
 return term(1,he+2);
}

//TODO do check in Worm
// 
//------------------------------------- Worm
class Worm
{
 public:
  class Listener
  {
   public:
    virtual ~Listener();
    virtual void oops()=0;
  };
  
 private:
  struct Segment;
  int wi,he;
  Term& term; //for drawing
  Segment* worm;
  int dx,dy; //the current direction
  bool ok;   
  Listener& li;
  void setDir(int dx,int dy);
  void check();
    
 public:
           Worm(int width,int height,Term& term,Listener& li);
  virtual ~Worm();
  void move();
  void move(int dx,int dy);
  void add();
  void reset();
};



//------------------------------------- Worm::Segment
struct Worm::Segment
{
 Term& term;
 int x;
 int y;
 Segment* next; 
          Segment(Term& term,int xp,int yp);
 virtual ~Segment();
 void set();
 void clear();
};

Worm::Segment::Segment(Term& term,int xp,int yp)
:term(term)
,x(xp)
,y(yp)
,next(this)
{
}

Worm::Segment::~Segment()
{
 clear();
}

void Worm::Segment::set()
{
 term(x,y)<<'o';
}

void Worm::Segment::clear()
{
 term(x,y)<<' ';
}

//------------------------------------- Worm::Listener
Worm::Listener::~Listener()
{
}

//------------------------------------- Worm
Worm::Worm(int width,int height,Term& term,Listener& li)
:wi(width),he(height)
,term(term)
,worm(new Segment(term,wi/2,he/2))
,dx(0),dy(1)
,ok(true)
,li(li)
{
 worm->set();
}

Worm::~Worm()
{
 Segment* s=worm;
 do
 {
  Segment* sn=s->next;
  delete s;
  s=sn;  
 }while(s!=worm);
}

void Worm::check()
{
 if (!ok) return;
 int x0=worm->x; //position of head
 int y0=worm->y;
 //limits
 ok=(0<=x0)&&(x0<wi)&&(0<=y0)&&(y0<he);
 Segment* s=worm->next;
 while((s!=worm)&&ok)
 {
  ok=!((x0==s->x)&&(y0==s->y));
  s=s->next;
 }
 if (!ok) li.oops();
}

void Worm::move(int dx,int dy)
{
 setDir(dx,dy);
 move();
}

void Worm::move()
{
 if (!ok) return;
 Segment* s=worm->next;
 s->clear();
 s->x=worm->x+dx;
 s->y=worm->y+dy;
 worm=s;
 worm->set();
 check();
}

void Worm::add()
{
 if (!ok) return;
 Segment* s=new Segment(term,worm->x+dx,worm->y+dy);
 s->next=worm->next;
 worm->next=s;
 worm=s;
 worm->set();
 check();
}

void Worm::setDir(int dx,int dy)
{
 this->dx=dx;
 this->dy=dy;
}

void Worm::reset()
{
 worm->clear();
 Segment* s=worm->next;
 while(s!=worm)
 {
  Segment* sn=s->next;
  delete s;
  s=sn;
 }
 worm->next=worm;
 worm->x=wi/2;
 worm->y=he/2;
 worm->set();
 ok=true;
}

//------------------------------------- Game
class Game:public Term::Listener,
           public Timer::Listener,
	   public Worm::Listener
{
 private:
  static const unsigned WI=16;
  static const unsigned HE=16;
  static const unsigned ADD=2; //every second tick
  Term  term;
  Timer ticker;
  Board board;
  Worm  worm;
  unsigned tick;
  
//-------------------------- implementation Timer::Listener  
  void onTick();

//-------------------------- implementation Term::Listener
  void onUp();
  void onDown();
  void onLeft();
  void onRight();
  void onEnter();
  
//-------------------------- implementation Worm::Listener
  void oops();
  
 public:
           Game();
  virtual ~Game();
};

Game::Game()
:term("/dev/tty",this)
,ticker(Timer::SECOND/2,this)
,board(WI,HE,term)
,worm(WI,HE,term,*this)
,tick(0)
{
 SOS::start(&term,&ticker);
}

Game::~Game()
{
 term<<"game over\n";
}

void Game::onTick()
{
 tick++;   
 if (tick==ADD)
    {
     tick=0;
     worm.add();
    }
    else
    {
     worm.move();
    }
}

void Game::onUp()
{
 worm.move(0,-1);
}

void Game::onDown()
{
 worm.move(0,1);
}

void Game::onLeft()
{
 worm.move(-1,0);
}

void Game::onRight()
{
 worm.move(1,0);
}

void Game::onEnter()
{
 board.clear();
 worm.reset();
}

void Game::oops()
{
 board.statusLine()<<"oops <enter> to restart";
}

int main(unsigned argc,char* args[])
{
 Game game;
}
