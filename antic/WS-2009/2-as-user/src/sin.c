/*------------------
 sin
 (c) Hans Buchmann FHNW 2009
 $Id$
 calling sin samplerate frequency duration
  samplerate Hz
  frequency  Hz
  duration   sec
 -------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

double arg(char* s) /* converts a string to a double */
{
 char* end;
 double v=strtod(s,&end);
 if (*end) 
    {
     fprintf(stderr,"%s not a number\n",s);
     exit(1);
    }
 return v;   
}

void usage(char* name)
{
 fprintf(stderr,"usage: %s samplerate[Hz] frequency[Hz] duration[sec]\n",name);
 exit(1);
}

int main(int argc,char** args)
{
 double fs      =44100;      /* Hz */
 double f       =  440;      /* Hz */
 double duration=    1;     /* sec */
 if (argc!=4) usage(args[0]);
 fs      =arg(args[1]);
 f       =arg(args[2]);
 duration=arg(args[3]);
 fprintf(stderr,"samplerate=%f Hz "
                "frequency =%f Hz "
		"duration  =%f sec\n",
		fs,f,duration);
 double alfa=2*M_PI*f/fs;
 double co=cos(alfa);
 double si=sin(alfa);
 double x=0;
 double y=1;
 double t=0;
 double dt=1.0/fs;
 while(t<duration)
 {
  int sample=(1<<30)*x; 
  write(STDOUT_FILENO,&sample,sizeof(sample));
                             /* 4 bytes */
  double x1=co*x-si*y;
  double y1=si*x+co*y;
  x=x1;
  y=y1;
  t+=dt;
 }
 return 0;
}
  
