#include <unistd.h>
#include <signal.h>
#include <iostream>
using namespace std;

void handler(int sig)
{
 cerr<<"Signal "<<sig<<"\n";
}

int main(unsigned argc,char* args[])
{
 ::signal(SIGALRM,handler);
 ::alarm(1);
 unsigned ch;
 cin>>ch;
 return 0;
}
