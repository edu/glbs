/*--------------------
 printf.c
 (c) H.Buchmann FHNW 2009
 $Id$
----------------------*/
#include <stdio.h>
int main(int argc,char** args)
{
 char   s[]="a string";
 int    val=8; 
 double pi=3.1415;
 char   ch='c';
 
 printf("s=%s val=%d (0x%x) pi=%f ch=%c\n",
            s,   val,  val, pi,   ch);
 return 0;
}
  
