/*-----------------
  output-stream
  (c) H.Buchmann FHNW 2009
  $Id$
-------------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 FILE* f=fopen("output.txt","w");
 if (f==0)
    {
     perror("fopen");
     return 1;
    }
 char s[]="Hello World";
 fputs(s,f);     /* string */
 fputc('\n',f);  /* char */
 fprintf(f,"s=%s\n",s); /* formatted */
 fclose(f);
 return 0;
}
  
  
