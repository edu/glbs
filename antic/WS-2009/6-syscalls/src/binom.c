/*----------------------
 binom
 (c) H.Buchmann FHNW 2008
 $Id$
------------------------*/
#include <stdio.h>
/*
TODO: proper formatting
*/
void binom(long long unsigned* line,unsigned len)
{
 for(unsigned l=0;l<len;++l)
 {
  unsigned i=l;
  line[i]=1;
  while(i>1)
  {
   --i; //i>0
   line[i]=line[i]+line[i-1];
  }
  
  for(unsigned i=0;i<=l;++i)
  {
   printf("%d ",line[i]);
  }
  printf("\n");
 }
}

int main(int argc,char** args)
{
 long long unsigned d[31];
 binom(d,30);
}
  
