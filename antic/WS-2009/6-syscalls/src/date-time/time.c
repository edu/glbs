/*-------------------
  time
  (c) H.Buchmann FHNW 2009
  $Id$
---------------------*/
#include <stdio.h>
#include <time.h>

int main(int arrgc,char** args)
{
 time_t t=time(0);
 printf("t=%d\n",t);
 struct tm* lt=localtime(&t);
 printf("%d:%d:%d\n",lt->tm_hour,lt->tm_min,lt->tm_sec);
 
 printf("%s\n",asctime(lt));
 return 0; 
}
  
