//-------------------------
//alarm example for call-back
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include <unistd.h>
#include <signal.h>
#include <iostream>
using namespace std;

void handler(int sig)
{
 cerr<<"Signal "<<sig<<"\n";
}

int main(unsigned argc,char* args[])
{
 ::signal(64,handler); 
 ::signal(63,handler);
 cerr<<"pid = "<<::getpid()<<"\n";
 while(true)
 {
  char ch;
  cin>>ch;
 }
 return 0;
}
