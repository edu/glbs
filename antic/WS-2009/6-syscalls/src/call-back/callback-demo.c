/*----------------
 callback-demo
 (c) H.Buchmann FHNW 2008
 $Id$
------------------*/
#include <stdio.h>
#include <unistd.h>

/*----------------------------------------------- os-section */
typedef void (Handler)(char);                   /* signature */
                            /* java interface ActionListener */

Handler* handler=0;                   /* variable initialized*/

void setHandler(Handler* h)
{
 handler=h;
}

void osSimulation()
{
 printf("---------- os-start\n");
 while(1)      /* forever */
 {
  char ch;         /* character */
  int len=read(STDIN_FILENO,&ch,1);   /* waits until one char read */
            /* |           |   |----the number of bytes sizeof(ch) */
            /* |           |------------------------ address of ch */
            /* |--------------------------------- FileId for stdin */ 
  
  if (handler!=0) handler(ch); /* call-back */
 } 
 printf("------------ os-end\n");
}
 
/*--------------------------------------------- user-section */
extern void mySecondHandler(char ch);
void myHandler(char ch) /* java implementation of ActionListener */
{
 printf("myHandler %c\n",ch);
 setHandler(mySecondHandler);
}

void mySecondHandler(char ch) /* java implementation of ActionListener */
{
 printf("mySecondHandler %c\n",ch);
 setHandler(myHandler);
}



int main(int argc,char** args)
{
 setHandler(myHandler);  /* java addListener */
 osSimulation();
 return 0;
}  
