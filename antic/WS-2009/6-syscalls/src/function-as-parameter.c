/*-------------------
  function-as-parameter
  (c) H.Buchmann  FHNW 2007
  $Id$
  -------------------*/
#include <stdio.h>

double aFunctionA(double v)
{
 printf("aFunctionA\n");
 return v*v;
}

double aFunctionB(double v)
{
 printf("aFunctionB\n");
 return 0.5*v;
}

typedef double (*Function)(double);

void caller(Function f)  /* function parameter */
{
 printf("calling f=%f\n",f(2));
}


int main(unsigned argc,char** args)
{
 Function func[]={aFunctionB,aFunctionA,0}; /* definition */
                                 /* function used as data */
 unsigned i=0;
 while(1)
 {
  Function f=func[i++];
  if (f==0) break;
  caller(f);
 }
}
  
