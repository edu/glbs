/*----------------
 io-demo
 (c) H.Buchmann FHNW 2008
 $Id$
------------------*/  
#include <stdio.h>        /* standard input/output library */

int main(int argc,char** args)
{
 FILE* fil=fopen("xxx.txt","r");       /* for reading */ 
 if (fil==0)
    {
     printf("error opening file\n");
     return 1;          /* leave main with error code */
    }
 /* reading */
 while(1)                                  /* forever */
 {
  int ch=fgetc(fil);   /* read one byte see man fgetc */
  if (ch==EOF) break;          /* end of file reached */
  printf("%c",ch);            /* print int ch as char */
 }
 fclose(fil);
 return 0;
}
