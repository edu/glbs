//--------------
//sine-demo
//(c) H.Buchmann FHNW 2008
//$Id$
//--------------
#include <iostream>
#include <math.h>

int main(int argc,char** args)
{
 double x=M_PI;
 std::cerr<<"sin("<<x<<")="<<sin(x)<<"\n";
 return 0;
}
