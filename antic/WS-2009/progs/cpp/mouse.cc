//-------------------------
//mouse
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include <iostream>
#include <iomanip>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

class Mouse
{
 private:
  int id;
  void read();
    
 public:
  Mouse(const char device[]);
  ~Mouse();
};

Mouse::Mouse(const char device[])
:id(-1)
{
 id=::open(device,O_RDONLY);
 if (id<0)
    {
     ::perror("open");
     ::exit(1);
    }
 read();   
}

Mouse::~Mouse()
{
 if (id>0) ::close(id);
}

//ps2 style
void Mouse::read()
{
 int x=0;
 int y=0;
 std::cerr<<"starting Left Key ends\n";
 for(;;)
 {
  char byte[3];

  ::read(id,byte,sizeof(byte));
  x+=(int)byte[1];
  y+=(int)byte[2];
#if 0
  for(unsigned i=0;i<3;i++)
  {
   std::cerr<<std::hex<<(unsigned)byte[i]<<": ";
  }
#endif
  std::cout<<std::dec<<x<<"\t"<<y<<"\n";
  if (byte[0]&1) break;
 }
 std::cerr<<"done\n";
}

int main(unsigned argc,char* args[])
{
 if (argc!=2)
    {
     std::cerr<<"usage "<<args[0]<<" mouseDevice\n";
     ::exit(1);
    }
 Mouse mouse(args[1]);   
}
