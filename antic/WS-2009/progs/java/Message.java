//--------------------------
//Message
//(c) H.Buchmann FHNW 2006
//$Id$
//--------------------------
import java.net.*;
import java.io.*;


class Listener
{
 public static final int PORT=5454;
 
 class Sink implements Runnable
 {
  private Thread th=new Thread(this);
  private LineNumberReader src;
  private String source;
  
  Sink(Socket sock) throws Exception
  {
   source=""+sock.getInetAddress();
   src=new LineNumberReader(
              new InputStreamReader(sock.getInputStream())
                           );
   th.start();
  }
  
  public void run()
  {
   try
   {
    while(true)
    {
     String li=src.readLine();
     if (li==null) break;
     System.out.println(source+" >"+li);
    }
   }
   catch(Exception ex)
   {
    System.err.println(ex);
   }
  }
  
 }

 Listener(int port) throws Exception
 {
  ServerSocket server=new ServerSocket(port);
  while(true)
  {
   Socket con=server.accept();
   new Sink(con);
  }
 }
 
 static public void main(String args[])
 {
  try
  {
  if (args.length==0) 
     {
      new Listener(PORT);
      System.exit(0);
     }
  if (args.length==1)
     {
      try
      {      
       new Listener(Integer.parseInt(args[0]));
       System.exit(0);
      }
      catch(NumberFormatException ex)
      {
       System.err.println(args[0]+": not a number");
       System.exit(1);
      }
     }
  }
  catch(Exception ex)
  {
   System.err.println(ex);
   System.exit(1);
  }
     
  System.err.println("usage: Server [port]");
  System.exit(1);    
 }
}

class  Talker
{
 Talker(String host,int port) throws Exception
 {
  Socket con=new Socket(host,port);
  LineNumberReader src=new LineNumberReader(
                             new InputStreamReader(System.in)
			                   );
  PrintWriter dst=new PrintWriter(
                   new OutputStreamWriter(con.getOutputStream()),
		   true //autoflush
                                 );
  while(true)
  {
   System.out.print(con.getInetAddress()+":");
   String li=src.readLine();
   dst.println(li);
  }				 
 }
 
 static public void main(String args[])
 {
  try
  {
   if (args.length==1)
      {
       new Talker(args[0],Listener.PORT);
       System.exit(0);
      }
   if (args.length==2)
      {
       try
       {      
	new Talker(args[0],Integer.parseInt(args[1]));
	System.exit(0);
       }
       catch(NumberFormatException ex)
       {
	System.err.println(args[0]+": not a number");
	System.exit(1);
       }
      }
  }
  catch(Exception ex)
  {
   System.err.println(ex);
   System.exit(1);
  }
  System.err.println("usage: Client host [port]");
  System.exit(1);    
 }
}
