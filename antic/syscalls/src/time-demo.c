/*--------------
  time-demo
  (c) H.Buchmann FHNW 2007
  $Id$
  --------------*/
#include <time.h>
#include <stdio.h>

int main(unsigned argc,char** args)
{
 time_t ti=time(0);
              /*^ NULL Pointer */
 printf("ti=%u\n",ti);
 struct tm* lt=localtime(&ti);
 /*      || |  |         | |----- variable ti         */
 /*      || |  |         |------- address of operator */
 /*      || |  |----------------- function            */
 /*      || |-------------------- name                */
 /*      ||---------------------- pointer             */
 /*      |----------------------- type struct tm      */
 printf("lt->tm_sec %d\n",lt->tm_sec);
 return 0;
}
  
