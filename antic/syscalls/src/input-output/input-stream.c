/*-----------------
  input-stream
  (c) H.Buchmann FHNW 2009
  $Id$
-------------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 FILE* f=fopen("output.txt","r");
 if (f==0)
    {
     perror("fopen");
     return 1;
    }
 while(1)
 {
  int ch=fgetc(f); /* 0<=ch<256 ch ascii code */
                   /* otherwise EOF end of file */
  if (ch==EOF) break;
  printf("%c",ch);
 }
 fclose(f);
 return 0;
}
  
  
