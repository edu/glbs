/*---------------
  typedef-demo
  (c) H.Buchmann FHNW 2007
  $Id$
  ---------------*/
#include <stdio.h>

typedef                    /* declaration */
struct
{
 double x; /* x-coordinate */
 double y; /* y-coordinate */
} Point;

void showIt(Point* p)
{
 printf("(%f,%f)\n",p->x,p->y);
                  /* dereference */
}

int main(unsigned argc,char** args)
{
 Point q={3,4};
 showIt(&q);
 return 0;
}
  
