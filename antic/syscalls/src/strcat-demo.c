/*---------------
  strcat-demo
  (c) H.Buchmann FHNW 2007
  $Id$
  ---------------*/
#include <stdio.h>
#include <string.h>

int main(unsigned argc,char** args)
{
 char s1[]="xx";
 char s2[]="yyyy";
 char res[80];                   /* enough space */
 res[0]='\0';             /* res is empty string */
 strcat(res,s1);
 strcat(res,s2);             /* make a drawing ! */
 printf("s1,s2=%s\n",res);
 return;
}
  
