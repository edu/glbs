/*
----------------------
signal-handler-2
(c) H.Buchmann FHNW 2006
$Id$
----------------------
*/
#include <stdio.h>
#include <signal.h>

unsigned callN=0; /* global visible in handler and main */

void handler(int sign)
{
 callN++;
 printf("signal#%d=\n",sign);
}

int main(unsigned argc,char** args)
{
 signal(SIGUSR1,handler);
 signal(SIGUSR2,handler);
 printf("pid=%d\n",getpid());
 while(1)
 {
  int ch=fgetc(stdin);
  printf("%c callN= %d",ch,callN);
 }
 return 0;
}
