//-------------------------
//alarm example for call-back
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include <unistd.h>
#include <signal.h>
#include <iostream>
using namespace std;

void handler(int sig)  //call-back function
{
 printf("------ signal %d occurred\n",sig);
}

int main(unsigned argc,char* args[])
{
 ::signal(SIGALRM,handler);  //register
 ::signal(SIGINT, handler);
 ::alarm(2);
 
 unsigned ch;
 cin>>ch;
 return 0;
}
