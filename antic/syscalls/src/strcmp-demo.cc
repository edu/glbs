//----------------
//strcmp-demo
//(c) H.Buchmann FHNW 2008
//$Id$
//----------------
#include <iostream>
#include <string.h>

int main(int argc,char** args)
{
 char w1[]="aaaaaaaaaaaaaaaaaaaaaaaa";
 char w2[]="aaq";
 int res=strcmp(w1,w2);
 std::cerr<<"strcmp("<<w1<<","<<w2<<")="<<res<<"\n";
 return 0;
}
