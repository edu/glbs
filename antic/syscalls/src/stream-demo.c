/*--------------
  stream-demo
  (c) H.Buchmann  FHNW 2007
  $Id$
 ---------------*/
#include <stdio.h>                  /* fopen/fclose etc. */
int main(unsigned argc,char** args)
{
 FILE* file=fopen("xxx.foo","xxxxx"); 
 if (file==0)
    {
     perror("fopen");
     return 1; /* with error code */
    }  
 /* do something */
 while(1) /* forever */
 {
  int ch=fgetc(file);
        /* 0<=ch<256 ch is an ascii character */
                       /* ch==EOF end of file */
  if (ch==EOF) break; /* leave 'forever' loop */
  /* ch is an ascii character */
  printf("%c\n",ch); 
 }
 fclose(file); 
 return 0;           /* signal ok */
}
 
