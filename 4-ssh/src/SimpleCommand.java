//-----------------------------
//SimpleCommand
//(c) H.Buchmann FHNW 2011
//$Id$
//prints command name and the arguments
//-----------------------------
class SimpleCommand
{
 public static void main(String args[])
 {
  System.out.println(SimpleCommand.class.getName()); //the command name
  for(String s:args)
  {
   System.out.println(s);
  }
 }
}
