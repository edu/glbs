//----------------------
//TimeClient
//(c) H.Buchmann FHNW 2010
//$Id$
//see RFC 868
//----------------------
import java.io.DataInputStream;
import java.net.Socket;
import java.util.Date;

class TimeClient
{
 final static int PORT=37;
 private TimeClient(String ip) throws Exception
 {
  Socket sock=new Socket(ip,PORT);
  DataInputStream in=new DataInputStream(sock.getInputStream());
  int val=in.readInt();
  sock.close();
  long v1900=0xffffffffl&(long)val;
  long v1970=v1900-2208988800l;
               // see rfc 868
  System.err.println(new Date(v1970*1000));
 }

 public static void main(String args[]) throws Exception
 {
  new TimeClient(args[0]);
 }
}
