//---------------------------
//Echo
//(c) H.Buchmann FHNW 2010
//$Id$
//---------------------------
import java.net.Socket;
import java.net.ServerSocket;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
class Echo
{
 static final int PORT=31415;
//Echo server [port]  
//Echo client ip [port] msg

 static class Client
 {
  private Client(String ip,int port,String msg) throws Exception
  {
   Socket socket=new Socket(ip,port); //TCP Connection
   InputStream   in=socket.getInputStream();
   OutputStream out=socket.getOutputStream();
   PrintWriter dst=new PrintWriter(out);
   dst.println(msg);
   dst.flush();
   //listen for answer
   while(true)
   {
    int ch=in.read();
    if (ch<0) break;
    System.err.print((char)ch);
    if (ch=='\n') break;
   }
   socket.close();
  }
 }
 
 static class Server
 {
  static class Connection implements Runnable
  {
   private Thread th=new Thread(this);
   
   private Socket socket;
   
   Connection(Socket socket)
   {
    this.socket=socket;
    th.start();
   }

   public void run()
   {
    try
    {
    System.err.println("accept "+socket);
    InputStream   in=socket.getInputStream();
    OutputStream out=socket.getOutputStream();
    while(true)
    {
     int ch=in.read(); // a byte
     if (ch<0) break;  
     out.write(ch);
    }
    socket.close();
    }catch(Exception ex)
    {
     ex.printStackTrace();
    }
   }
  }
  
  private Server(int port) throws Exception
  {
   ServerSocket server=new ServerSocket(port);
   System.err.println("listening on port "+port);
   while(true)
   {
    new Connection(server.accept());
   } 
  }
 }
 
 public static void main(String args[]) throws Exception
 {
  switch(args.length)
  {
   case 1:
    if (!args[0].equals("server")) break;
    new Server(PORT);
   return;

   case 2: 
    if (!args[0].equals("server")) break;
    new Server(Integer.parseInt(args[1]));
   return;
   
   case 3:
    if (!args[0].equals("client")) break;
    new Client(args[1],PORT,args[2]);
   return;

   case 4:
    if (!args[0].equals("client")) break;
    new Client(args[1],Integer.parseInt(args[2]),args[3]);
   return;
   
  }
  System.err.println("usage Echo server [port]\n"+
                     "      Echo client ip [port] msg");  
 }
}
