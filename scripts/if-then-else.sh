#-----------------
#if-then-else.sh
#(c) H.Buchmann FHNW 2006
#$Id$
#-----------------
#see man test
X="1"
if [ $X = "1" ]
   then echo "true"
   else echo "false"
fi

if [ -f $1 ]
  then echo "is a file"
  else echo "is not a file"
fi
  
