#-----------------
#switch.sh
#(c) H.Buchmann FHNW 2006
#$Id$
#-----------------
case $1 in
 one)  echo "--one--";;
 two)  echo "--two--";;
 three)echo "--three--";;
 *)    echo "usage: $0 one|two|three";;
esac
