/*-------------------
  thread-demo
  (c) H.Buchmann FHNW 2008
  $Id$
  compile/link with gcc -lpthread
---------------------*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void* myRunA(void* p)         /* java Runnable.run() */
{
 while(1)
 {
  printf("myRunA\n");
  for(unsigned i=0;i<(1<<15);++i){}      /* use cpu */
 }
 return 0;                    /* 0 is of type void* */
}

void* myRunB(void* p)         /* java Runnable.run() */
{
 while(1)
 {
  printf("myRun----------------------------B\n");
  for(unsigned i=0;i<(1<<15);++i){}      /* use cpu */
 }
 return 0;                    /* 0 is of type void* */
}

int main(int argc,char** args)
{
 pthread_t th0;   /* defined but not (yet) initalized */
 pthread_create(&th0,        /* set by pthread_create */
                   0,           /* default attributes */
              myRunA,             /* the Runnable.run */
		   0);               /* no parameters */

 
 pthread_t th1;
 pthread_create(&th1, 
                   0,
              myRunB,
		   0);



 int ch=getc(stdin);              /* wait for a char */
 return 0;
}
  
