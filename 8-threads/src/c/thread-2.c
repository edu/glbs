/*
-----------------------------
thread-1
(c) H.Buchmann FHNW 2006
$Id$
-----------------------------
*/
#include <pthread.h>
#include <stdio.h>

unsigned val=0;  //accessible by everybody
pthread_mutex_t mutex;

void* run(void* p)
{
 unsigned id= *(unsigned*)p;
 printf("init %d\n",id);
 while(1)
 {
  pthread_mutex_lock(&mutex);
  unsigned l=val; //copy 
  l++;
  if (l!=val+1) printf("-- %d\n",id);
  val=l;
  pthread_mutex_unlock(&mutex);
 }
 return 0; //never called
}

int main(unsigned argc,char** args)
{
 pthread_mutex_init(&mutex,0);
 pthread_t th1;
 pthread_t th2;
 unsigned id0=0;
 pthread_create(&th1,0,run,&id0);
 unsigned id1=1;
 pthread_create(&th2,0,run,&id1);

 
 getc(stdin);
 printf("val= %d\n",val);
 return 0;
}

