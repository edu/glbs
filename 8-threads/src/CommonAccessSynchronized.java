//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2013
//$Id$
//TODO write access functions inc/dec in Pool
//----------------
class Pool
{
 public long v=0;
}

abstract class Agent implements Runnable
{
 final static long CNT=1l<<25;
 protected Pool pool;
 private Thread  th=new Thread(this);
 
 Agent(Pool pool)
 {
  this.pool=pool;
  th.start();
 }
 
 
 void join() throws Exception //called in main
 {
  th.join(); //wait until thread th terminates
 }
 
 abstract void doSomething(); //runs in thread
 public void run()
 {
  long cnt=0;
  while(cnt<CNT)
  {
   ++cnt;
   doSomething();
  }
 }
}

class Incrementer extends Agent
{
 Incrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  synchronized(pool)
  {
   long v=pool.v;
   v=v+1;
   pool.v=v;
  }
 }
}

class Decrementer extends Agent
{
 Decrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  synchronized(pool)
  {
   long v=pool.v;
   v=v-1;
   pool.v=v;
  }
 }
}

class CommonAccess
{
 public static void main(String args[]) throws Exception
 {
  Pool pool =new Pool(); //one instance
  Agent inc=new Incrementer(pool);
  Agent dec=new Decrementer(pool);
  dec.join();  //run in parallel
  inc.join();
  System.err.println("pool="+pool.v);
 }
}
