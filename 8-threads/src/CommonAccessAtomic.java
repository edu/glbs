//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2013
//$Id$
//TODO write access functions inc/dec in Pool
//----------------
import java.util.concurrent.atomic.AtomicLong;

class Pool
{
 public AtomicLong v=new AtomicLong(); //before long v;
}

abstract class Agent implements Runnable
{
 final static long CNT=1<<20;
 protected Pool pool;
 private Thread  th=new Thread(this);
 
 Agent(Pool pool)
 {
  this.pool=pool;
  th.start();
 }
 
 
 void join() throws Exception //called in main
 {
  th.join(); //wait until thread th terminates
 }
 
 abstract void doSomething(); //runs in thread
 public void run()
 {
  long cnt=0;
  while(cnt<CNT)
  {
   ++cnt;
   doSomething();
  }
 }
}

class Incrementer extends Agent
{
 Incrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  pool.v.incrementAndGet(); //++pool.v *not* synchronized 
 }
}

class Decrementer extends Agent
{
 Decrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  pool.v.decrementAndGet(); //--pool.v *not* synchronized 
 }
}

class CommonAccess
{
 public static void main(String args[]) throws Exception
 {
  Pool pool =new Pool(); //one instance
  Agent inc=new Incrementer(pool);
  Agent dec=new Decrementer(pool);
  dec.join();
  inc.join();
  System.err.println("pool="+pool.v);
 }
}
