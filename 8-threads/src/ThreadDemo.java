//------------------
//ThreadDemo
//(c) H.Buchmann FHNW 2008
//$Id$
//------------------
class MyRun implements Runnable
{
 private int id;                      //identification
 
 private Thread th=new Thread(this);  

 MyRun(int id)
 {
  this.id=id;  //runs in main
  System.err.println("MyRun "+id);
  th.start();
 }
 
//implementation of Runnable 
 public void run()
 {
  while(true) //runs in thread
  {
   System.err.println(id); //uncomment for heavy cpu load
  }
 }
}

class ThreadDemo
{
 static public void main(String args[])
 {
  for(int i=0;i<4;++i) new MyRun(i);
 }
}
