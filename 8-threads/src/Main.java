//--------------------
//Main.java
//(c) H.Buchmann FHNW 2018
//--------------------
class Main
{
 private static void doSomething()
 {
  System.out.println("do something");
 }
 public static void main(String args[])
 {
  doSomething(); //call function
 }
}
