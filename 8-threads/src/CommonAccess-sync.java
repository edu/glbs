//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2008
//$Id$
//----------------
class Pool
{
 public int v=0;
}

class MyRun extends Thread// implements Runnable 
{
 private Pool pool;  //reference to Pool 
 private Thread  th=new Thread(this);

 MyRun(Pool pool)
 {
//  super(this);
  this.pool=pool;
  th.start();     //of thread
 }
 
 public void run()
 {
  int cnt=1<<24;
  while(true)
  {
   synchronized(pool)
   {
    int l=pool.v;
    ++l;
    while(cnt>0) --cnt; // wait 
    if (l!=(1+pool.v)) System.err.println("-o-");
    pool.v=l;
   }
  }
 }
}

class CommonAccess
{
 
 public static void main(String args[]) throws Exception
 {
  Pool pool =new Pool(); //one instance
  MyRun run0=new MyRun(pool);
  MyRun run1=new MyRun(pool); 
 }
}
