//------------------
//ConsumerProducer 
//(c) H.Buchmann FHNW 2007
//$Id$
//------------------

class Item          //the thing to be produced/consumed
{
 final Producer prod;
 final long val;
 Item(Producer prod,
           long val)
 {
  this.prod=prod;
  this.val=val;
 }
 
 public String toString()
 {
  return ""+val;
 }
}

class Pool      //for the somethings
{
 private Item theItem;

 private long putCnt=0; 
 private long getCnt=0;
 Pool()
 {
  theItem=null;
 }
 
 boolean isFull()
 {
  return theItem!=null;
 }
 
 boolean isEmpty()
 {
  return theItem==null;
 }
 
 void put(Item it)  //for the Producer
 {
  while(theItem!=null)
  {
  }
  theItem=it;
  ++putCnt;
 }
 
 Item get()
 {
  while(theItem==null)
  {
  } 
  Item it=theItem;  //local copy of the reference
  theItem=null;
//  System.out.println("get");
  ++getCnt;
  return it;
 }
 
 void  balance() //called in main thread
 {
  System.out.println("pool balance="+(getCnt-putCnt));
 }
}

abstract class Agent implements Runnable
{
 protected String id;
 protected Pool pool; //reference
 private Thread th=new Thread(this);
 private static final long CNT=(1<<16);
  
 protected Agent(String id,Pool pool)
 {
  this.id=id;
  this.pool=pool;
  th.start();
 }
 
 abstract void doIt();
 abstract void balance();
 
 public void run()
 { 
  long cnt=CNT;
  while(cnt>0)
  {
   --cnt;
   doIt();
  }
 }
 
 void join() throws Exception //called in main thread
 {
  th.join();
  System.err.println("'"+id+"' joined");
 }
}

class Producer extends Agent
{
 private long val=0;
 Producer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
 }

 void doIt()
 {
  Item it=new Item(this,val);
  pool.put(it);
  ++val;
 } 
 
 void balance()
 {
  System.out.println("Producer "+id+": "+val+" items");
 }
}

class Consumer extends Agent
{
 private long consumed=0;
 Consumer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
 }
 
 void doIt()
 { 
  Item it=pool.get();
  ++consumed;
 }

 void balance()
 {
  System.out.println("Consumer "+id+": items "+consumed);
 }
}

//the main class
class  ConsumerProducer
{
 public static void main(String args[]) throws Exception
 {
  Pool pool=new Pool(); //one instance
  Agent a1=new Producer("p0",pool);
  Agent a0=new Consumer("c0",pool);
  System.err.println("run");
  a0.join();
  a1.join();
  a0.balance();
  a1.balance(); 
  pool.balance();
 }
}
