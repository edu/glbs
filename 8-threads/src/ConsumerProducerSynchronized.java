//------------------
//ConsumerProducer 
//(c) H.Buchmann FHNW 2007
//$Id$
//------------------

class Something //the thing to be produced/consumed
{
 private String val;
 Something(String val)
 {
  this.val=val;
 }
 
 public String toString()
 {
  return val;
 }
}

class Pool      //for the somethings
{
 private volatile Something theThing; //empty or full
 private volatile long cnt;
 Pool()
 {
  theThing=null;
 }
 
 synchronized void put(Something sth)
 { 
  try
  {
   while(theThing!=null)  //wait until empty
   {
    wait();
   }
   theThing=sth;
   notifyAll();
  }
  catch(Exception ex)
  {
   ex.printStackTrace();
   System.exit(0);
  }
 }
 
 synchronized Something get()
 {
  try
  {
   while(theThing==null) //wait until not empty
   {
    wait();
   }
   Something sth=theThing;
   cnt=1l<<20;
   while(cnt>0){--cnt;}
   theThing=null; //now empty
   notifyAll();
   return sth;
  }
  catch(Exception ex)
  {
   ex.printStackTrace();
   System.exit(0);
   return null;
  }
 }
 
}

abstract class Agent implements Runnable
{
 protected String id;
 protected Pool pool; //reference
 private Thread th=new Thread(this);
 
 protected Agent(String id,Pool pool)
 {
  this.id=id;
  this.pool=pool;
 }
 
 protected void start()
 {
  th.start();
 } 
}

class Producer extends Agent
{
 private int id=0;
 Producer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
  start();
 }
 
 //implementation Runnable
 public void run()
 { 
  while(true)
  {
   Something sth=new Something(super.id+":"+id);
   pool.put(sth);
   ++id;
  }
 }
}

class Consumer extends Agent
{
 Consumer(String id,Pool pool)
 {
  super(id,pool); //initialize superclass
  start();
 }
 
 public void run()
 { 
  while(true)
  {
   Something sth=pool.get();
   System.err.println(sth);
  }
 }
}

//the main class
class  ConsumerProducer
{
 public static void main(String args[])
 {
  Pool pool=new Pool(); //one instance
  new Consumer("c0",pool);
  new Producer("p0",pool);
  new Consumer("c1",pool);
 }
}
