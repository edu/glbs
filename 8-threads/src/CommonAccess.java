//----------------
//CommonAccess
//(c) H.Buchmann FHNW 2013
//$Id$
//----------------
class Pool
{
 public long v=0;
}

abstract class Agent implements Runnable
{
 final static long CNT=1<<16;
 final static long IDLE=1<<18;
 protected Pool pool;
 private Thread  th=new Thread(this);
 
 Agent(Pool pool)
 {
  this.pool=pool;
  th.start();
 }
 
 
 void join() throws Exception //called in main
 {
  th.join(); //wait until thread th terminates
 }
 
 abstract void doSomething(); //runs in thread

 public void run()
 {
  long cnt=0;
  while(cnt<CNT)
  {
 //  for(long i=0;i<IDLE;++i) ++i; //wait
   ++cnt;
   doSomething();
  }
 }
}

class Incrementer extends Agent
{
 Incrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  synchronized(pool)
  {
   long v=pool.v; //copy into local
   v=v+1;         //do it
   pool.v=v;      //copy into global;
  }
 }
}

class Decrementer extends Agent
{
 Decrementer(Pool pool)
 {
  super(pool);
 }
 
 void doSomething()
 {
  synchronized(pool)
  {
   long v=pool.v; //copy into local
   v=v-1;         //do it
   pool.v=v;      //copy into global;
  }
 }
}

class CommonAccess
{
 public static void main(String args[]) throws Exception
 {
  Pool pool =new Pool(); //one instance
  Agent inc=new Incrementer(pool); //thread
  Agent dec=new Decrementer(pool); //thread

  dec.join();
  inc.join();
  
  System.err.println("pool="+pool.v); //both inc/dec terminate
 }
}
