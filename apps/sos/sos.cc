//----------------------------
//SOS Simple Operating System
// example of event driven programming
// (c) H.Buchmann FHNW 2006
//$Id$
//----------------------------
#include "sos.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

const char Term::ESC[]="\e[";

const Term::ToAction Term::Action_[]=
      {
       {"\n"  ,&Listener::onEnter},
       {"\e[A",&Listener::onUp},
       {"\e[B",&Listener::onDown},
       {"\e[C",&Listener::onRight},
       {"\e[D",&Listener::onLeft},
       {     0,0} //terminates list
      };

Term::Listener::~Listener()
{
}

Term::Term(const char device[],Listener* li)
:th(this)
,id(-1)
,li(li)
,escapeN(0)
{
 id=::open(device,O_RDWR);
 if (id<0)
    {
     ::perror("open");
     ::exit(1);
    }
 if (!isatty(id))
    {
     std::cerr<<"device '"<<device<<"' is not a terminal\n";
     ::exit(1);
    } 
 ::tcgetattr(id,&spec);
 termios spec1; //the new one
 ::tcgetattr(id,&spec1);
 spec1.c_lflag&=~(ECHO|ICANON);
 ::tcsetattr(id,TCSANOW,&spec1);
}

Term::~Term()
{
 if (id>=0) 
    {
     ::tcsetattr(id,TCSANOW,&spec);
     ::close(id);
    }
}

void Term::run()
{
 while(true)
 {
  char ch;
  ::read(id,&ch,sizeof(ch));
  onChar(ch);
 }
}

void Term::emptyEscape()
{
 if (li)
    {
     for(unsigned i=0;i<escapeN;i++) li->onKey(escape[i]);
    }
 escapeN=0;   
}

void Term::onChar(char ch)
{
 escape[escapeN++]=ch;
 unsigned i=0;
 while(true)
 {
  const ToAction& a=Action_[i++];
  if (a.code==0)
     {
      emptyEscape();
      return;
     }
  unsigned k=0; //index 
  while(true)
  {
   if (k==escapeN)
      {
       if (a.code[k]=='\0') 
          {
	   if (li) (li->*a.act)();
	   escapeN=0;
	  }
       return;	  
      }
   if (escape[k]!=a.code[k])
      {
       break;
      }
   k++;   
  }
 }
}

Term& Term::clear()
{
 std::cerr<<ESC<<"2J"<<ESC<<"H";
 return *this;
}

Term& Term::operator()(unsigned x,unsigned y)
{
 std::cerr<<ESC<<(y+1)<<';'<<(x+1)<<'H';
 return *this;
}

Term& Term::up()
{
 std::cerr<<ESC<<'A';
 return *this;
}

Term& Term::down()
{
 std::cerr<<ESC<<'B';
 return *this;
}

Term& Term::left()
{
 std::cerr<<ESC<<'D';
 return *this;
}

Term& Term::right()
{
 std::cerr<<ESC<<'C';
 return *this;
}

Timer::Listener::~Listener()
{
}

Timer::Timer(unsigned delta_us,Listener* li)
:th(this)
,delta(delta_us)
,li(li)
{
// std::cerr<<"Timer\n";
// th.start();
}

Timer::~Timer()
{
}

void Timer::run()
{
 
 while(true)
 {
  ::usleep(delta);
  if (li) li->onTick();  
 }
}

void SOS::start(Term* term,Timer* timer)
{
 if (term) term->th.start();
 if (timer) timer->th.start();
 ::pause();
}
