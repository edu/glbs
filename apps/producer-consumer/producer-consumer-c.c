/*
--------------------------------
producer-consumer-c
(c) H.Buchmann FHNW 2006
$Id$
--------------------------------
*/
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
/*--------------------------- producer/consumer */
 typedef struct
 {
  char* name;
  unsigned id;
  pthread_t th;
  unsigned total;
  unsigned count;
 }Agent;
 
 void showAgent(Agent* p)
 {
  printf("%s id=%d total=%d count=%d\n",p->name,p->id,p->total,p->count);
 }
 
 Agent prod0;
 Agent prod1;
 Agent cons0;
 Agent cons1;
 
/*--------------------------- Initsemaphore */
 sem_t initS;           /* initially closed */
 
 
/*--------------------------- Product */
typedef struct
{
 unsigned manufacturer;     /* the id */
 unsigned value;            /* simply a number */
} Product;

/*--------------------------- Pool */
pthread_mutex_t productM;
volatile Product* product=0;          /* a pointer */

void* produce(void* arg)
{
 Agent* self=(Agent*)arg;
 sem_post(&initS);
 printf("producer %d\n",self->id);
 unsigned value=0;
 
 while(1)
 {
  self->total++;
  pthread_mutex_lock(&productM);
  if (product==0)
    {
     self->count++;
      product=malloc(sizeof(Product));
     product->manufacturer=self->id;
     product->value       =value;
     value++;                       /* the next product */
    }
  pthread_mutex_unlock(&productM);
 }
}

void* consume(void* arg)
{
 Agent* self=(Agent*)arg;
 sem_post(&initS);
 printf("consumer %d\n",self->id);
 while(1)
 {
  self->total++;
  pthread_mutex_lock(&productM);
  if (product!=0)
    {
     self->count++;
     free(product);
     product=0;     /* consumed */
   }
  pthread_mutex_unlock(&productM);
 }
}

void createProducer(Agent* ag,unsigned id)
{
 ag->name="producer";
 ag->id=id;
 ag->count=0;
 ag->total;
 pthread_create(&ag->th,0,produce,ag);
 sem_wait(&initS);
}

void createConsumer(Agent* ag,unsigned id)
{
 ag->name="consumer";
 ag->id=id;
 ag->count=0;
 ag->total;
 pthread_create(&ag->th,0,consume,ag);
 sem_wait(&initS);
}

int main(unsigned argc,char** args)
{
 sem_init(&initS,0,0);
 pthread_mutex_init(&productM,0);
 createProducer(&prod0,0);
// createProducer(&prod1,1);
 createConsumer(&cons0,0);
 createConsumer(&cons1,1);

 getc(stdin);
// sem_wait(&initS);
 
 sem_destroy(&initS);
 showAgent(&prod0);
// showAgent(&prod1);
 showAgent(&cons0);
 showAgent(&cons1);
 return 0;
}
