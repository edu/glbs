//------------------------
//producer-consumer
//(c) H.Buchmann FHNW 2006
//$Id$
//------------------------
#include "thread.h"
#include <iostream>

//------------------------------- Agent
class Agent:public Runnable
{
 protected:
  Thread th;
  char* name; 
  unsigned id;
  unsigned total;
  unsigned count;
  Agent(char name[],unsigned id);
  
 public:
  virtual ~Agent();
  void show();
  void start();
};

Agent::Agent(char n[],unsigned idN)
:th(this)
,name(n)
,id(idN)
,total(0)
,count(0)
{

}

Agent::~Agent()
{
}

void Agent::show()
{
 std::cerr<<name<<" "<<id<<" total/count = "<<total<<"/"<<count<<"\n";
}

void Agent::start()
{
 th.start();
}

//------------------------------- Product
class Product
{
 private:
  
  Agent* manufacturer;
  unsigned value;
  
 public:
           Product(Agent* manufacturer,unsigned val);
  virtual ~Product(); 
  void show();
};

Product::Product(Agent* mf,unsigned val)
:manufacturer(mf)
,value(val)
{
}

Product::~Product()
{
}

Mutex    productM;
Product* product=0; //common

//------------------------------- Producer
class Producer:public Agent
{
 private:
  void run();
 public:
           Producer(unsigned id);
  virtual ~Producer();
};

Producer::Producer(unsigned id)
:Agent("Producer",id)
{
 start();
}

Producer::~Producer()
{
}

void Producer::run()
{
 show();
 while(true)
 {
  total++;
  productM.lock();
  if (product==0)
     {
      product=new Product(this,count); //consume
      count++;
     }
  productM.unlock();
 }
}

//------------------------------- Consumer
class Consumer:public Agent
{
 private:
  void run();
 public:
           Consumer(unsigned id);
  virtual ~Consumer();
};

Consumer::Consumer(unsigned id)
:Agent("Consumer",id)
{
 start();
}

Consumer::~Consumer()
{
}

void Consumer::run()
{
 show();
 while(true)
 {
  total++;
  productM.lock();
  if (product!=0)
     {
      count++;
      delete product; //consume
      product=0;
     }
  productM.unlock();
 }
}

int main(unsigned argc,char* args[])
{
 Consumer cons(0);
 Producer prod(1);
 char ch;
 std::cin>>ch;
 cons.show();
 prod.show();
 return 0; 
}
