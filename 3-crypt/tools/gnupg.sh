#!/bin/bash
#--------------------------
#gnupg.sh
#(c) H.Buchmann FHNW 2016
#--------------------------
#we are in 3-crypt
#directories done with tree --charset=ascii
#.
#|-- build
#|   |-- gnupg-2.1.15
#|   |-- openssl-1.0.2j
#|   `-- pinentry-0.9.6
#|-- progs <---------- using --prefix
#|   |-- bin
#|   |-- include
#|   |-- lib
#|   |-- libexec
#|   |-- sbin
#|   |-- share
#|   `-- ssl
#|-- tools

PREFIX=${PWD}/progs
GNUPG=gnupg-2.1.15
cd build/${GNUPG}

./configure --prefix=${PREFIX}
make -j8
make install
