#!/bin/bash
#--------------------------
#openssl.sh
#(c) H.Buchmann FHNW 2016
#--------------------------
#we are in 3-crypt
#directories done with tree --charset=ascii
#.
#|-- build
#|   |-- gnupg-2.1.15
#|   |-- openssl-1.0.2j
#|   `-- pinentry-0.9.6
#|-- progs <---------- using --prefix
#|   |-- bin
#|   |-- include
#|   |-- lib
#|   |-- libexec
#|   |-- sbin
#|   |-- share
#|   `-- ssl
#|-- tools

PREFIX=${PWD}/progs
OPENSSL=openssl-1.0.2j
cd build/${OPENSSL}

./config --prefix=${PREFIX}
make -j8
make test
make install
