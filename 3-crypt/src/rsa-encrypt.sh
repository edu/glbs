#!/bin/bash
#----------------------
#rsa-encrypt.sh
#(c) H.Buchmann FHNW 2018
# stdin: plaintext
# stdout: ciphertext
#----------------------
openssl rsautl -raw -inkey public.key -pubin -encrypt
