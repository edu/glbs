#!/bin/bash
#----------------------
#rsa-encrypt.sh
#(c) H.Buchmann FHNW 2018
#----------------------
openssl rsautl -raw -inkey private.key -decrypt
