#-----------------
#receive.sh
#(c) H.Buchmann FHNW 2012
#$Id$
# decode and verify msg.hash msg.txt
# ${1} sender
#-----------------
openssl enc -d -des-ede -k 1234 < msg.txt > theMessage
   #    |symmetric key
openssl enc -d -des-ede -k 1234 < msg.hash |
openssl rsautl -verify -inkey ${1}/public.key -pubin >theHash
   
