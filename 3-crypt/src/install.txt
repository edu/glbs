openssl.org

install --prefix
config --prefix 
see INSTALL
make
make test
make install

rand

encoding/decoding symmetric
---------------------------

base64
../bin/openssl base64  -in test.txt | ../bin/openssl base64 -d

des
../bin/openssl  enc -des -in test.txt  -k 1234 > test.enc
../bin/openssl  enc -des -in test.enc  -d -k 1234
../bin/openssl  enc -des -in test.txt  -k 1234 | ../bin/openssl enc -des -d -k 1234
write scripts

digest signing
------

../bin/openssl dgst -sha test.txt


rsa
---
genrsa 
rsa
rsautl
