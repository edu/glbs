//--------------------------
//SimpleRSA
//(c) H.Buchmann FHNW 2011
//$Id$
//--------------------------
class SimpleRSA
{
 private static void usage()
 {
  System.err.println("usage: SimpleSymmetric encode|decode key");
  System.exit(1);
 }
 
 
 SimpleRSA(String key) throws Exception 
 {
  String[] k=key.split(":");
  int n=Integer.decode(k[0]);
  int p=Integer.decode(k[1]);
  while(true)
  {
   int ch=System.in.read();
   if (ch<0) break;
   int chout=(ch+p)%n;
   System.out.write(chout);
  }
  System.out.close();
 }
 
 
 public static void main(String args[]) throws Exception 
 {
  if (args.length!=2) usage();
  if (args[0].equals("encode")) 
     {
      new SimpleRSA(args[1]);  
      return;
     }
  if (args[0].equals("decode")) 
     {
      new SimpleRSA(args[1]);  
      return;
     }
  usage();
 }
}
