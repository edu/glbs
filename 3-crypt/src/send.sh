#-------------------
#send.sh
#(c) H.Buchmann FHNW 2012
#$Id$
# use case
# send.sh recipient msg
#  ${1} recipient directory
#  ${2} msg file
#  all relevant files are in the current directory 
#-------------------
openssl dgst -sha1 -binary < ${2}          | #pipe 
openssl rsautl -sign    -inkey private.key | #pipe
openssl enc -e -des-ede -k 1234 > ${1}/msg.hash

openssl enc -e -des-ede -k 1234 < ${2} > ${1}/msg.txt
