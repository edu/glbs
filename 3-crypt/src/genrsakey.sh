#!/bin/bash
#----------------
#genrsakey.sh
#(c) H.Buchmann FHWN 2018
#creates private/public key 
# in current dir
#TODO private.key only accessible by owner
#----------------
openssl genrsa > private.key
openssl rsa -pubout < private.key >public.key

