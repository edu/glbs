//-------------------------
//PseudoRandom.java
//(c) H.Buchmann FHNW 2019
//-------------------------
import java.util.Random;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Color;
import javax.imageio.ImageIO;
import java.io.FileOutputStream;
 
class PseudoRandom
{
 private static final int WI=1024;
 private static final int HE=1024;
 private static final int R =101;
 
 private static void randomImage(int seed) throws Exception
 {
  Random random=new Random(seed); //try with/without seed
  BufferedImage img=new BufferedImage(WI,HE,BufferedImage.TYPE_INT_RGB);
  Graphics2D g=img.createGraphics();
  g.setBackground(Color.WHITE);
  g.clearRect(0,0,WI,HE);
  for(int i=0;i<128;++i)
  {
   int x=random.nextInt(WI);
   int y=random.nextInt(HE);
   int r=random.nextInt(R);
   Color c=new Color(random.nextInt(256),random.nextInt(256),random.nextInt(256));
   g.setColor(c);
   g.fillOval(x,y,r,r);
  }
  ImageIO.write(img,"png",System.out);
 }
 
 private static void randomNumbers(int seed)
 {
  Random random=new Random(seed);//try with/without seed
  for(int i=0;i<10;++i)
  {
   System.out.println(random.nextInt());
  }
 }

 private static void usage()
 {
  System.err.println("usage "+PseudoRandom.class.getName()+"seed numbers | image\n"+
                     "  seed=int number");
  System.exit(1);
 }
 
//calling
// java PseudoRandom seed (numbers | image)
 public static void main(String args[]) throws Exception
 {
  if (args.length!=2) 
     {
      usage();
     }
  try
  {   
   switch(args[1])
   {
    case "numbers":
     randomNumbers(Integer.decode(args[0])); 
    return;
    case "image":
     randomImage(Integer.decode(args[0]));
    return;
   }
  } catch(NumberFormatException ex)
  {
  }
  usage();
 }
}
