//-------------------------
//Image
//(c) H.Buchmann FHNW 2011
//$Id: Image.java 3427 2011-11-21 17:50:34Z buchmann $
//-------------------------
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import javax.imageio.ImageIO;

class Image
{
 static final String Format="gif";
 private Image() throws Exception
 {
  BufferedImage img=new BufferedImage(800,480,BufferedImage.TYPE_INT_RGB);
  Graphics g=img.getGraphics();
  g.setFont(g.getFont().deriveFont(128.0f));
  g.drawString(Integer.toHexString(hashCode()),20,200);
  System.err.println("format "+Format);
  ImageIO.write(img,Format,System.out);
  System.out.close();
 }
 
 public static void main(String args[]) throws Exception
 {
  new Image();
 }
}
