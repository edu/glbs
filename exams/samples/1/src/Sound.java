//---------------------------
//Sound
//(c) H.Buchmann FHNW 2010
//$Id: Sound.java 3428 2011-11-21 17:51:23Z buchmann $
//---------------------------
import java.io.DataOutputStream;
import java.io.LineNumberReader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.util.Vector;

//start tab dur tab amp tab freq 
class Sound
{
 static final double FS=44000; //Hz
 class Osc
 {
  private double start,end,amp,f; 
  private double cos,sin;
  private double x,y;
  private double t;
  private double dt;
    
  Osc(double start,
      double dur,
      double amp,
      double f)
  {
   this.start=start;
   this.end  =start+dur;
   this.amp  =amp;
   this.f    =f;
   double alfa=2*Math.PI*f/FS;
   cos=Math.cos(alfa);
   sin=Math.sin(alfa);
   x=amp;
   y=0;
   dt=1.0/FS;
  }
  
  double step()
  {
   t+=dt;
   if (t<start) return 0;
   if (t>end)   return 0;
   double v=y;
   double x1=cos*x-sin*y;
   double y1=sin*x+cos*y;
   x=x1;
   y=y1;
   return v;
  }
  
  boolean run(){return t<end;}
  
  public String toString()
  {
   return ""+start+"\t"+(end-start)+"\t"+amp+"\t"+f;
  }
 }
 
 private Vector<Osc> osc=new Vector<Osc>();
 
 static private int scale(double v) //-1<=v<=1
 {
  return (int)(Integer.MAX_VALUE*v);
 }
 
 private void play() throws Exception
 {
  System.err.println("sampling frequency "+FS);
  DataOutputStream out=new DataOutputStream(System.out);
  while(true)
  {
   boolean play=false;
   double s=0;
   for(Osc o:osc)
   {
    play|=o.run();
    s+=o.step();
   }
   out.writeInt(scale(s));
   if (!play) break;
  }
  out.close();
 }
 
 private void show()
 {
  for(Osc o:osc) System.err.println(o);
 }
 
 private void build() throws Exception
 {
  LineNumberReader in=new LineNumberReader(new InputStreamReader(System.in));
  while(true)
  {
   String li=in.readLine();
   if (li==null) break;
   System.err.println(li);
   li=li.trim();
   if (li.length()==0) continue;
   if (li.charAt(0)=='#') continue; //comment
   String p[]=li.split("\\s+");
   if (p.length!=4) 
      {
       throw new Exception("format error "+in.getLineNumber());
      }
   osc.add(new Osc(Double.parseDouble(p[0]), //start
                   Double.parseDouble(p[1]), //dur
		   Double.parseDouble(p[2]), //amp
		   Double.parseDouble(p[3])  //f
                  )
          );
           
  }
 }
 
 Sound() throws Exception
 {
  build();
//  show();
  play();
 }
 
 public static void main(String args[]) throws Exception
 {
  new Sound();
 }
}
