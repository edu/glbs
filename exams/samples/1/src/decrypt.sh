#-------------------------
#decrypt.sh
#(c) H.Buchmann FHNW 2011
#$Id: decrypt.sh 3427 2011-11-21 17:50:34Z buchmann $
#usage decrypt.sh public-key encrypted-message
#-------------------------
openssl rsautl -decrypt  -inkey ${1} < ${2}
