/*------------------
  hello-world.c
  (c) H.Buchmann FHNW 2017
  ------------------*/
#include <stdio.h>

int main(int argc,char** args[])
{
 printf("Hello World %s %s\n",__FILE__,__DATE__);
 return 0;
}
