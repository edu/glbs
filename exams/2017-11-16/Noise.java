//---------------------------
//Noise.java
//(c) H.Buchmann FHNW 2016
//---------------------------
import java.util.Random;
import java.io.DataOutputStream;
import java.io.IOException;
class Noise
{
 static final private int FS=22000;
 
 private static int scale(double x)
 {
  double y=x*Integer.MAX_VALUE;
  if (y>= Integer.MAX_VALUE) return +Integer.MAX_VALUE;
  if (y<=-Integer.MAX_VALUE) return -Integer.MAX_VALUE;
  return (int)y;
 }
 
 private Noise(double decay) throws IOException
 {
  DataOutputStream out=new DataOutputStream(System.out);
  Random rnd=new Random();
  double dt=1.0/FS;
  double f=Math.pow(0.5,1.0/(FS*decay));
  double a=2;
  double t=0;
  for(int i=0;i<5;++i)
  {
   for(double tt=0;tt<decay;tt+=dt)
   {
    double v=a*(rnd.nextDouble()-0.5);
    out.writeInt(scale(v)); 
//    System.out.println(t+"\t"+v);
    a*=f;
    t+=dt;
   }   
  }
  out.close();
 }
 
 private static void usage()
 {
  System.err.println("usage: "+Noise.class.getName()+" decay\n"+
                     "  decay:half life in seconds"); 
 }
 
 static public void main(String args[]) throws Exception
 {
  try
  {
   switch(args.length)
   {
    case 1:
     new Noise(Double.parseDouble(args[0]));
    return; 
   }
   usage();
  }
  catch(NumberFormatException ex)
  {
   usage();
  }
 }
}
