#!/bin/bash
#----------------------
#verify.sh
#(c) H.Buchmann FHNW 2017
#------------------------

gpg --no-default-keyring --keyring ${1} --verify ${2}

