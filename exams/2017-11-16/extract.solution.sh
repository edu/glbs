#!/bin/bash
#---------------------------
#extract.sh
#(c) H.Buchmann FHNW 2017
#---------------------------
unzip -p ${1}  | tail -n +3 | base64 -d | gzip -c -d |  pnmtojpeg -quality 10
