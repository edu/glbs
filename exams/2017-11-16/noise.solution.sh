#!/bin/bash
#-----------------
#noise.sh
#(c) H.Buchmann FHNW 2017
#-----------------
java Noise 0.0625 | sox  -B --bits 32 -e signed-integer -r 22000 -t raw - noise.ogg
