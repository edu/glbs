#!/bin/sh
#---------------------
#loop.sh
#(c) H.Buchmann FHNW 2019
#---------------------
CNT=0
while [ 1 ]
do
 echo ${CNT}
 sleep 1
 CNT=$((CNT+1))
done
