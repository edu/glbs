/*--------------------
  hello-world.c
  (c) H.Buchmann FHNW 2019
  create executable:
  gcc hello-world.c -static -o hello-world
 ---------------------*/
#include <stdio.h>

int main(int argc,char** args)
{
 printf("--  ---- hello-world --  --\n");
 return 0;
}
