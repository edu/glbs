//--------------------------
//Chatter
//(c) H.Buchmann FHNW 2020
//$Id$
//--------------------------
import java.io.LineNumberReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

class Chatter
{
 public static final int PORT=5454;  //the default PORT
 
 static class Listener
 {
  class Sink implements Runnable
  {
   private Thread th=new Thread(this);
   private ObjectInputStream src;
   private String source;

   Sink(Socket sock) throws Exception
   {
    source=""+sock.getInetAddress();
    src=new ObjectInputStream(sock.getInputStream());
    th.start();
   }

   public void run()
   {
    try
    {
     while(true)
     {
      System.out.println(source+" >"+src.readObject());
     }
    }
    catch(Exception ex)
    {
     System.err.println(ex);
    }
   }

  }

  Listener(int port) throws Exception
  {
   ServerSocket server=new ServerSocket(port);
   while(true)
   {
    Socket con=server.accept();
    new Sink(con);
   }
  }
 } //Listener

 static class  Talker
 {
  Talker(String host,int port) throws Exception
  {
   Socket con=new Socket(host,port);
   LineNumberReader   src=new LineNumberReader(
                                new InputStreamReader(System.in)
				      );
   ObjectOutputStream dst=new ObjectOutputStream(con.getOutputStream());
   while(true)
   {
    System.out.print(con.getInetAddress()+":");
    dst.writeObject(src.readLine());
    dst.flush();
   }				 
  }

 }//Talker
 
 //Chatter l[isten]  [port]    1|2 parameter
 //Chatter t[alk]    ip [port] 2|3 parameter
 public static void main(String args[]) throws Exception
 {
  switch(args.length)
  {
   case 1:
    if (args[0].startsWith("l")) 
       {
        new Listener(PORT);
	return;
       }
   break;
   case 2:
    if (args[0].startsWith("l"))
       {
	new Listener(Integer.parseInt(args[1]));
	return;
       }
    if (args[0].startsWith("t"))
       {
	new Talker(args[1],PORT);
	return;
       }
   break;
   case 3:
    if (args[0].startsWith("t"))
       {
        new Talker(args[1],Integer.parseInt(args[1]));
	return;
       }     
  }//end switch
  System.err.println("usage: Chatter l[isten] [port]\n"+
                     "               t[alk] ip [port]");
 }
}
