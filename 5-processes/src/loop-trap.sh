#!/bin/bash
#-----------------
#loop-trap.sh
#(c) H.Buchmann FHNW 2019
# use kill -signalname pid
#-----------------
echo "Starting '${0}' pid='${$}'"
#------------------------ action
trap 'echo "ring the bell"' PWR
trap 'echo "TERM--"'        TERM 
trap 'echo "USR1--";ls'     USR1  #two commands
#     |                     |-------- the signal
#     |------------------------------ the action


while [ true ]
do
 true
done

