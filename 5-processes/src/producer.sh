#!/bin/bash
#------------------
#producer.sh
#(c) H.Buchmann FHNW 2009
#use redirection
#------------------
COUNT=0
while [ true ]
do
 sleep 0.125
 echo "produce ${COUNT}" # to stdout
 
 COUNT=$((COUNT+1))
done
 
