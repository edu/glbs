//--------------------------
//RegularPolygon.java
//(c) H.Buchmann FHNW 2010
//$Id$
//calling java RegularPolygon numberOfVertices
// n num
// using gnuplot
// set size square
// set term pdf for pdf
// set term x11 for screen
// set output 'poly.pdf'
// plot 'poly.csv' wi li 
//--------------------------
class RegularPolygon
{
//TODO handle negative n
 private RegularPolygon(int n)
 {
  double alfa=2*Math.PI/n;
  double sin=Math.sin(alfa);
  double cos=Math.cos(alfa);
  double x0=1;
  double y0=0;
  double x=x0;
  double y=y0;
  while(n>0)
  {
   System.out.println(x+"\t"+y);
   double x1=cos*x-sin*y;
   double y1=sin*x+cos*y;
   x=x1;
   y=y1;
   --n;
  }
  System.out.println(x0+"\t"+y0);
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args.length!=1) 
     {
      throw new Exception("usage: RegularPolygon numberOfVertices");
             //output on cerr
     }
  new RegularPolygon(Integer.parseInt(args[0]));
 }
}

