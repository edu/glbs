#pragma once
//--------------------
//pen
//(c) H.Buchmann FHNW 2019
//--------------------
#include <iostream>
class Pen
{
 public:
   Pen(std::ostream& out,            //where to write the svg
       double x0,double y0,double len); //start position
  ~Pen();  
/*
 directions
 3     2     1
  \    |    /
   \   |   /
    \  |  /
     \ | /
      \|/
 4-----o-----0
      /|\
     / | \
    /  |  \
   /   |   \
  /    |    \
 5     6     7
*/  
 //all these methods returns c++ reference of myself 
  Pen& turn(unsigned dir); //0 1 2 3 4 5 6 7
  Pen& mov(); 
  Pen& up();
  Pen& down();
  

 private:
  std::ostream& out; 
  double x,y;    //the current pos
  unsigned d;    //current direction
  double dx[8];
  double dy[8];  //the current direction
  bool penDown;
  void open();   
  void close();
  void lineto();
};


