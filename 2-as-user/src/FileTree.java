//----------------------------
//FileTree
//(c) H.Buchmann FHNW 2019
//----------------------------
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Files;

class FileTree
{
 long cnt=0;
 private void visit(File r)
 {
  ++cnt;
  System.out.print("\""+r+"\"--{");
  File lst[]= r.listFiles();
  for(File f:lst) 
  {
   if (f.isDirectory()) System.out.println("\""+f+"\";");
  }
  System.out.println("}");  
  for(File f:lst)
  {
   Path p=f.toPath();
   if (!Files.isSymbolicLink(p) && f.isDirectory()) 
      {
       visit(f);
      }
      else
      {
       ++cnt;
      }
  }
 }
 
 private FileTree(String r)
 {
  File root=new File(r);
  if (!root.isDirectory()) 
     {
      System.err.println(r+" not a directory");
      System.exit(1);
     }
  System.out.println("graph \"filetree\" {");
  System.out.println("node[shape=none;label=\".\"]");
//  System.out.println("node[shape=none]");   
  visit(root);
  System.out.println("}");
  System.err.println("cnt="+cnt);
 }

 public static void main(String args[])
 {
  new FileTree(args[0]);
 }
}
