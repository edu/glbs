//-----------------------
//Sum
// reads numbers from stdin and 
// prints the sum to stdout
//(c) H.Buchmann FHNW 2011
//$Id$
//calling Sum hex|dec 
//           hex: numbers in hexadecimal 
//           dec: umbers in  decimal 
//-----------------------
import java.io.LineNumberReader;
import java.io.InputStreamReader;
class Sum
{
 static class Parser
 {
  private int radix;
  Parser(int radix)
  {
   this.radix=radix;
  }
  
  long parse(String s) throws NumberFormatException
  {
   return Long.parseLong(s,radix);
  }
 }
 
 
 private static void usage()
 {
  System.err.println("usage: Sum hex|dec\n"+
                     "       hex: numbers hexadecimal\n"+
		     "       dec: numbers decimal");
  System.exit(1);
 }

 private Sum(Parser p) throws Exception
 {
  LineNumberReader src=new LineNumberReader(new InputStreamReader(System.in));
  long sum=0;
  while(true)
  {
    String li=src.readLine();
    if (li==null) break;
    for(String s:li.split("\\s+"))
    {
     try
     {
      sum+=p.parse(s);
     }
     catch(NumberFormatException ex)
     {
      System.err.println(src.getLineNumber()+":"+s+" not a number");
      System.exit(1);
     }
    }
  }
  System.out.println(sum);
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args.length!=1) usage();
  if (args[0].equals("hex")) 
      {
       new Sum(new Parser(16));
       return;
      }
  if (args[0].equals("dec")) 
      {
       new Sum(new Parser(10));
       return;
      }
  usage();     
 }
}
