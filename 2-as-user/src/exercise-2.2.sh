#------------------
#exercise-2.2.sh
#pipes
#(c) H.Buchmann FHNW 2013
# ${1} name of picture assuming jpg
#------------------
jpegtopnm ${1} | pnmtojpeg -quality=20 > output.jpg
         #|    |
         #|    |            |           |-- redirection
         #|    |            |-------------- option
	 #|    |--------------------------- pipe                  
         #|-------------------------------- jpeg picture
