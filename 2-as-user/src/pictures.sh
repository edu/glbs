#!/bin/bash
#------------------------
#pictures.sh
#(c) H.Buchmann FHNW 2019
#------------------------
#pngtopnm ../src/figure1.png | pnminvert | pnmtojpeg > figure1.jpg
#pngtopnm ../src/figure1.png | pnmsmooth -width=11 -height=5 | pnmtojpeg > figure1.jpg
#pngtopnm ../src/figure1.png | pnmsmooth -width=11 -height=5 | pnmtile 500 400 | pnmtojpeg > figure1.jpg
