#!/bin/bash
#---------------------
#play.sh
#(c) H.Buchmann FHNW 2015
# usage sh play.sh file
#byte        0        1        2        3
#bits in byte
#                76543210 76543210 76543210 76543210
#one sample     |                 |
#signed integer |fedcba98 76543210|
#---------------------
play --type=raw \
     --bits=16 \
     --encoding=signed-integer \
     --rate=44k \
	 ${1}

