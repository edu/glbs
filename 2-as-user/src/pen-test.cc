//--------------------
//pen-test
//(c) H.Buchmann FHNW 2019
//--------------------
#include "pen.h"
#include <iostream>
class Koch
{
 public:
  Koch();
 private:
  Pen p;
  void draw(unsigned deep);
  void moveturn(unsigned d);
};

Koch::Koch()
:p(std::cout,10,200,3)
{
 p.down();
 p.turn(0);
 draw(5);
 p.mov();
}

void Koch::moveturn(unsigned d)
{
 p.mov().turn(d);
}


void Koch::draw(unsigned deep)
{
 if (deep==0) return;
 --deep;
 draw(deep);moveturn(1);
 draw(deep);moveturn(6);
 draw(deep);moveturn(1);
 draw(deep);
}

class Peano
{
 public:
  Peano();
 private:
  Pen p;
  void draw(unsigned deep);
  void moveturn(unsigned d0,unsigned d1);
};

void Peano::moveturn(unsigned d0,unsigned d1)
{
 p.mov().turn(d0).mov().turn(d1);
}

void Peano::draw(unsigned deep)
{
 if (deep==0) 
    {
     return;
    }
 
 --deep;
 draw(deep);moveturn(1,1);
 draw(deep);moveturn(7,7);
 draw(deep);moveturn(7,7);
 draw(deep);moveturn(7,7);
 draw(deep);moveturn(1,1);
 draw(deep);moveturn(1,1);
 draw(deep);moveturn(1,1);
 draw(deep);moveturn(7,7);
 draw(deep);
}

Peano::Peano()
:p(std::cout,1000,550,4)
{
 p.down();
 p.turn(4);
 draw(4);
 p.mov();
}


void test()
{
 Pen p(std::cout,200,200,30);
 p.down();
 for(unsigned i=0;i<8;++i) p.turn(1).mov();
 p.up().mov().mov().mov().down();
 for(unsigned i=0;i<8;++i) p.turn(1).mov();
}

int main(int argc,char** args)
{
 Peano();
// test();
 return 0; 
}

