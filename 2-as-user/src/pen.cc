//--------------------
//pen
//(c) H.Buchmann FHNW 2019
//--------------------
#include "pen.h"
Pen::Pen(std::ostream& out,double x0,double y0,double len)
:out(out)
,x(x0),y(y0)
,d(0)
,penDown(false)
{

 dx[0]= len;dx[1]=len;dx[2]=   0;dx[3]=-len;dx[4]=-len;dx[5]=-len;dx[6]=   0;dx[7]= len;
 dy[0]=   0;dy[1]=len;dy[2]= len;dy[3]= len;dy[4]=   0;dy[5]=-len;dy[6]=-len;dy[7]=-len; 

 out<<"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
      "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\"\n"
        	 "\"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">\n"
      "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
	  " xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";

//      "<path d='M"<<x<<" "<<y<<"\n";	  
}


Pen::~Pen()
{
 if (penDown) close();
 out<<" </svg>\n";
}

Pen& Pen::turn(unsigned dd)
{
 d+=dd;
 d%=8;
 return *this;
}

Pen& Pen::mov()
{
 x+=dx[d];
 y+=dy[d];
 if (penDown) lineto();
 return *this;
}

Pen& Pen::up()
{
 if (penDown) close();
 penDown=false;
 return *this;
}

Pen& Pen::down()
{
 if (!penDown) open();
 penDown=true;
 return *this;
}

void Pen::open()
{
 out<<"<path d='M"<<x<<" "<<y<<"\n";
}

void Pen::close()
{
 out<<"\n' stroke='black' fill='none'/>\n";
}

void Pen::lineto()
{
 out<<"L"<<x<<" "<<y;
}
