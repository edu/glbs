#------------------
#exercise-2.2.sh
#pipes
#(c) H.Buchmann FHNW 2013
#------------------
java Sine 880 2 | play --no-show-progress \
 --channels  1 \
 --type raw \
 --rate 44000 \
 --encoding signed-integer \
 --bits 32  \
 --endian big \
 -
