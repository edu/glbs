#------------------
#exercise-2.1.sh
# first before first use 
#  javac RegularPolygon.java
# redirection
#(c) H.Buchmann FHNW 2013
# ${1} number of vertices
# csv tabbed text
#------------------
java RegularPolygon ${1} > polygon.csv
#                        |redirection
#useful for excel,matlab,gnuplot
# excel
#  open file
# matlab
#  load file 
# gunplot  
#   plot 'polygon.csv' with line
