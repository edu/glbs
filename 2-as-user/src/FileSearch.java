//---------------------------
//FileSearch didactic example
//(c) H.Buchmann FHNW 2013
//calling java -Dpath=p1:p2:p3  FileSearch files
//---------------------------
import java.io.File;

class FileSearch
{
 private String[] pathList; 

 private void pathList()
 {
  pathList=System.getProperty("path").split(":");
 }
 
 private void search(String name)
 {
  System.out.print(name+":");
  for(String d:pathList)
  {
   File f=new File(d,name);
   if (f.exists()) 
      {
       System.out.print(f);
       break;
      }   
  }
  System.out.println();
 }
 
 private FileSearch(String nameList[])
 {
  pathList();
  for(String name:nameList)
  {
   search(name);
  }
 }
 
 public static void main(String args[])
 {
  new FileSearch(args);
 }
}
