//----------------------------
//Number
//(c) H.Buchmann FHNW 2010
//$Id$
//see state machine on approx p13 on slides
//TODO .calculate value of pattern
//     .negative numbers
//     .hexadecimal numbers 
//----------------------------
class Number
{
 private boolean parse(String pattern)
 {
  int i=0;       //index in pattern
  int status=0;
  while(true)
  {
   char ch=(i<pattern.length())?pattern.charAt(i):'\0';
           //ch=='\0' signalizes end of string
   ++i; //next 
   switch(status)
   {
    case 0:
     if (Character.isDigit(ch)){status=1;break;}
    return false;
    
    case 1:
     if (Character.isDigit(ch)){break;} //remain in status 1
    return (ch=='\0'); 
   }//switch 	   
  }
 }
 
 private Number(String pattern)
 {
  System.err.println("parse("+pattern+")="+parse(pattern));
 }
 
 public static void main(String args[])
 {
  if (args.length==1)
     {
      new Number(args[0]);
      return;
     }
  System.err.println("usage: Number pattern");
 }
}

