#------------------
#show-code.sh
#(c) H.Buchmann FHNW 2009
#$Id$
#------------------
#parameter ${1} binary file
#          ${2} offset in file 
if [ ! ${#} -eq 2 ]
 then echo "usage ${0} binaryFile offset"
      exit 1
fi

objdump --target=binary\
        --architecture=i8086\
	--disassemble-all\
	--start-address=${2}\
	${1}
