#!/bin/bash
#-------------------
#copy-bios.sh to stdout
# sudo copy-bios.sh 
#(c) H.Buchmann FHNW 2012
#-------------------
#000f0000-000fffff : System ROM 
# see cat /proc/iomem
# you must be root
BLOCKSIZE=512 
BIOS_START=0xf0000
BIOS_SIZE=0x10000 
dd if=/dev/mem skip=$((BIOS_START/BLOCKSIZE)) count=$((BIOS_SIZE/BLOCKSIZE)) 
