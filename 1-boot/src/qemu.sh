#!/bin/bash
#---------------------
#qemu.sh
#(c) H.Buchmann FHNW 2012
#$Id$
#${1} the boot floppy
#---------------------
#using -no-fd-bootchk so we can boot with exotic (non standard) floppies
BIOS=/usr/share/qemu/bios.bin
if [ ! ${#} -eq 1 ]
   then echo "${0} bootFloppy"
        exit 1
fi
#---------------- boot from virtual floppy
qemu-system-x86_64 -no-fd-bootchk \
                   -fda ${1}

