#!/bin/bash
#--------------------
#make-boot.sh
#(c) H.Buchmann FHNW 2011
#$Id$
#calling sh make-boot.sh srcFile
#--------------------
if [ -z ${1} ]
   then echo "usage ${0} srcFile"
        exit 1
fi
SRC=${1}
SRC0=${SRC%.*}             #remove extension
gcc -c ${1} -o ${SRC0}.o &&  #compile\\
ld --entry=0 --oformat=binary ${SRC0}.o -o ${SRC0}.img  #link
echo "*** image stored in file: ${SRC0}.img"
echo "*** use './qemu.sh ${SRC0}.img' to run image"
